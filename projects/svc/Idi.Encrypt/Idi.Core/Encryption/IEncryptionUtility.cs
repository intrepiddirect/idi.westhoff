﻿namespace Idi.Core.Encryption
{
    public interface IEncryptionUtility {
        string Encrypt(string value);
        string Decrypt(string value);
        string Encrypt(string value, string salt);
        string Decrypt(string value, string salt);
        string Encrypt(string value, string password, string salt);
        string Decrypt(string value, string password, string salt);
    }
}