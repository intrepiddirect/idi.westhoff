﻿namespace Idi.Core.Encryption
{
    public class PassThroughFormattingEncrypter : IFormattingEncryptor
    {
        public T Encrypt<T>(T value, EncryptionFormat format)
        {
            return value;
        }

        public T Decrypt<T>(T value, EncryptionFormat format)
        {
            return value;
        }
    }
}