﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Idi.Core.Encryption
{

    public class EncryptionUtility : IEncryptionUtility
    {
        private const string Key = "?zdwH#W$v-;s-bUKdd#J29S6X1E243";
        private const string Salt = "IDIMKT";

        public string Encrypt(string value)
        {
            return Encrypt(value, Key, Salt);
        }

        public string Decrypt(string value)
        {
            return Decrypt(value, Key, Salt);
        }

        public string Encrypt(string value, string salt)
        {
            return Encrypt(value, Key, salt);
        }

        public string Decrypt(string value, string salt)
        {
            return Decrypt(value, Key, salt);
        }

        public string Encrypt(string value, string password, string salt)
        {
            if (value == null)
                return null;

            var encryptionAlgorithm = new AesManaged();

            var deriveBytes = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt), 1);

            // size in bits shifted right by 3 = size in bytes
            byte[] key = deriveBytes.GetBytes(encryptionAlgorithm.KeySize >> 3);

            // this IV will always be the same for a given session.  We used to vary it for every encryption, but that will cause the client to fail, because ID's that are foriegn keys will not match up with respective ids on the resources that they reference.
            byte[] initializationVector = deriveBytes.GetBytes(encryptionAlgorithm.BlockSize >> 3);

            ICryptoTransform transform = encryptionAlgorithm.CreateEncryptor(key, initializationVector);

            using (var buffer = new MemoryStream())
            {
                using (var stream = new CryptoStream(buffer, transform, CryptoStreamMode.Write))
                using (var writer = new StreamWriter(stream, Encoding.Unicode))
                {
                    writer.Write(value);
                }
                return Convert.ToBase64String(buffer.ToArray());
            }
        }

        public string Decrypt(string value, string password, string salt)
        {
            if (value == null)
                return null;

            var encryptionAlgorithm = new AesManaged();

            var deriveBytes = new Rfc2898DeriveBytes(password, Encoding.Unicode.GetBytes(salt), 1);

            // size in bits shifted right by 3 = size in bytes
            byte[] key = deriveBytes.GetBytes(encryptionAlgorithm.KeySize >> 3);
            byte[] initializationVector = deriveBytes.GetBytes(encryptionAlgorithm.BlockSize >> 3);

            ICryptoTransform transform = encryptionAlgorithm.CreateDecryptor(key, initializationVector);
            using (var buffer = new MemoryStream(Convert.FromBase64String(value)))
            using (var stream = new CryptoStream(buffer, transform, CryptoStreamMode.Read))
            using (var reader = new StreamReader(stream, Encoding.Unicode))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
