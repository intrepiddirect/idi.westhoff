﻿namespace Idi.Core.Rules
{
    public class RuleResult
    {
        public RuleOutcome Result { get; set; }
        public int? ReasonId { get; set; }
        public int? ItemId { get; set; }
        public string EntityName { get; set; }
        public string Comments { get; set; }
        public string RuleName { get; set; }
    }
}