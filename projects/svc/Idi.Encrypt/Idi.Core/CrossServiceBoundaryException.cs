﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Idi.Core
{
    /// <summary>
    /// Throw a service error exception to indicate that the error message that you are conveying is safe to return across a service boundary.
    /// an exception of this type must not include sensitive information.
    /// Inner exceptions may contain sensitive information.  
    /// Inner exceptions will not be included in the transomission result
    /// </summary>
    public class CrossServiceBoundaryException : Exception
    {
        public CrossServiceBoundaryException()
        {
        }

        public CrossServiceBoundaryException(string message) : base(message)
        {
        }

        public CrossServiceBoundaryException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CrossServiceBoundaryException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
