﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Idi.Core
{
    public static class Extensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }

        public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
        {
            return !enumerable.Any();
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key) => GetValueOrDefault(dictionary, key, default(TValue));
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            TValue result;
            return dictionary.TryGetValue(key, out result) ? result : defaultValue;
        }

        /// <summary>
        /// Useful for retrieving values from App.config, returns a config value as a nullable bool
        /// </summary>
        public static bool GetBool(this NameValueCollection settings, string key)
        {
            string value = settings[key];
            return !string.IsNullOrEmpty(value) && bool.Parse(value);
        }

        public static long GetLong(this NameValueCollection settings, string key)
        {
            var value = settings[key];
            long parsedValue = 0;
            long.TryParse(value, out parsedValue);
            return parsedValue;
        }

        public static string ToCommaDelimited(this IEnumerable<string> enumerable)
        {
            return string.Join(",", enumerable);
        }
        
        public static bool ContainsAny<TSource>(this IEnumerable<TSource> source, IEnumerable<TSource> value,
            IEqualityComparer<TSource> comparer = null)
        {
            if (comparer == null)
                comparer = EqualityComparer<TSource>.Default;
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            return (from x in source from y in value where comparer.Equals(x, y) select x).Any();
        }

        public static string PropertyListToString(this object obj)
        {
            var props = obj.GetType().GetProperties();
            var sb = new StringBuilder();
            sb.AppendLine($"{obj.GetType().Name}:");
            foreach (var p in props)
            {
                sb.AppendLine($"{p.Name} : {p.GetValue(obj, null)}");
            }
            return sb.ToString();
        }

        /// <summary>
        /// A base 64 string alone will still error out if posted in a URL.  WebUtility.UrlEncode/Decode corrupts the base64 such that decoding it fails
        /// See this so response to see our solution: http://stackoverflow.com/a/26354677/67038
        /// </summary>
        /// <param name="base64String">The value.</param>
        /// <returns></returns>
        public static string SafeEncodeBase64(this string base64String)
        {
            return base64String?.TrimEnd('=')
                .Replace('+', '-')
                .Replace('/', '_');
        }

        /// <summary>
        /// Takes a safe encoded base 64 string and reverses it back to a base 64 string
        /// See this SO response to see our solution: http://stackoverflow.com/a/26354677/67038
        /// </summary>
        /// <param name="encodedString">The value.</param>
        /// <returns></returns>
        public static string SafeDecodeBase64(this string encodedString)
        {
            if (encodedString == null)
                return null;

            var decoded = encodedString.Replace('_', '/')
                .Replace('-', '+');

            switch (encodedString.Length % 4)
            {
                case 2:
                    decoded += "==";
                    break;
                case 3:
                    decoded += "=";
                    break;
            }
            return decoded;
        }

        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (value.Length > maxLength)
            {
                value = value.Substring(0, maxLength);
            }
            return value;
        }

        public static DateTime? AddYear(this DateTime? dateTime)
        {
            return dateTime.HasValue ? dateTime.Value.AddYears(1) : dateTime;
        }

        public static bool IsBetweenMinOrMax(this DateTime dateTime)
        {
            return dateTime > DateTime.MinValue && dateTime < DateTime.MaxValue;
        }

        public static string NullIfEmptyOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null : value;
        }

        public static string RemoveWhitespace(this string input)
        {
            return Regex.Replace(input, @"\s+", "");
        }

        public static string RemoveAllButAlphaCharacters(this string str)
        {
            return Regex.Replace(str, "[^a-zA-Z]+", "", RegexOptions.Compiled);
        }

        public static string RemoveLineBreaks(this string input)
        {
            //Replacing with space because there is a possibility that we'll be dealing with two words on different lines
            //Don't want to have two words inadvertently jammed together 
            return Regex.Replace(input, @"\r\n?|\n", " ");
        }

        public static bool IsInRange(this Range range, decimal? value)
        {
            return value.HasValue
                   && (range.Min == null || value.Value >= range.Min)
                   && (range.Max == null || value.Value <= range.Max);
        }

        public static decimal GetValueWithinRangeOrDefault(this Range range, decimal value)
        {
            var min = (range.Min ?? value);
            var max = (range.Max ?? value);

            return value < min ? min : value > max ? max : value;
        }
    }
}