﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Idi.Core.StringExtensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// This extension breaks strings into a list of strings. Any words that are longer than the lineLength will be placed on their own line.
        /// </summary>
        /// <param name="text">Original text</param>
        /// <param name="lineLength">Max length of each string (line) in the list after processing. Any words that are longer than the lineLength will be placed on their own line.</param>
        /// <returns></returns>
        public static List<string> BreakIntoLines(this string text, int lineLength)
        {
            if (text.Length < lineLength)
            {
                return new List<string> { text };
            }
            var stringBuilder = new StringBuilder();
            var accumulator = new List<string>();


            if (text.HasLineEndings())
            {
                var lineBreaks = text
                    .ReplaceLineEndings("\n")
                    .Split(new[] { "\n" }, StringSplitOptions.None);

                foreach (var lineBreak in lineBreaks)
                {
                    accumulator.AddRange(lineBreak.BreakIntoLines(lineLength));
                }

                return accumulator;
            }

            var words = text.Split(' ');
            foreach (var word in words)
            {
                if (word.Length <= lineLength)
                {
                    if (stringBuilder.Length + word.Length < lineLength)
                    {
                        AddTextToStringBuilder(stringBuilder, word);
                    }
                    else
                    {
                        AddTextToLine(accumulator, stringBuilder);
                        AddTextToStringBuilder(stringBuilder, word);
                    }
                }
                else
                {
                    if (stringBuilder.Length > 0)
                    {
                        // clear out the existing string builder and add it 
                        AddTextToLine(accumulator, stringBuilder);
                    }
                    // only one word fits into this line
                    accumulator.Add(word);
                }
            }
            if (stringBuilder.Length > 0)
            {
                // if there is any data left in the string builder then write it to the accumulator
                AddTextToLine(accumulator, stringBuilder);
            }
            return accumulator;
        }

        public static bool HasLineEndings(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            var lineSeparator = ((char)0x2028).ToString();
            var paragraphSeparator = ((char)0x2029).ToString();

            return value.Contains("\r\n") ||
                value.Contains("\n") ||
                value.Contains("\r") ||
                value.Contains(lineSeparator) ||
                value.Contains(paragraphSeparator);
        }

        public static string ReplaceLineEndings(this string value, string replacementValue)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            var lineSeparator = ((char)0x2028).ToString();
            var paragraphSeparator = ((char)0x2029).ToString();

            return value.Replace("\r\n", replacementValue)
                .Replace("\n", replacementValue)
                .Replace("\r", replacementValue)
                .Replace(lineSeparator, replacementValue)
                .Replace(paragraphSeparator, replacementValue);
        }

        private static void AddTextToStringBuilder(StringBuilder sb, string word)
        {
            sb.Append(word);
            sb.Append(" ");
        }

        private static void AddTextToLine(List<string> accumulator, StringBuilder sb)
        {
            accumulator.Add(sb.ToString().Trim());
            sb.Clear();
        }

        public static decimal? GetNullableDecimalFromString(string dec)
        {
            return decimal.TryParse(dec, out var de) ? de : (decimal?)null;
        }

        public static T? ToValueOrNull<T>(this string s) where T : struct
        {
            var result = new T?();
            try
            {
                if (!string.IsNullOrEmpty(s) && s.Trim().Length > 0)
                {
                    var conv = TypeDescriptor.GetConverter(typeof(T));
                    result = (T)conv.ConvertFrom(s);
                }
            }
            catch { }
            return result;
        }

        public static string GetEmailCopyright()
        {
            return $"&copy; {DateTime.Now.Year}";
        }
    }
}