﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Idi.Core
{
    public static class NumeralExtension
    {
        //converted from http://stackoverflow.com/a/22039673
        public static string ToRomanNumeral(this int number)
        {
            var numberRomanDictionary = new Dictionary<int, string>
            {
                {1000, "M"},
                {900, "CM"},
                {500, "D"},
                {400, "CD"},
                {100, "C"},
                {50, "L"},
                {40, "XL"},
                {10, "X"},
                {9, "IX"},
                {5, "V"},
                {4, "IV"},
                {1, "I"},
            };

            var roman = new StringBuilder();

            foreach (var item in numberRomanDictionary)
            {
                while (number >= item.Key)
                {
                    roman.Append(item.Value);
                    number -= item.Key;
                }
            }

            return roman.ToString();
        }

        public static bool IsRomanNumeral(this string value)
        {
            return value.All(c => "IVXLCDM".Contains(c));
        }

        // converted from http://stackoverflow.com/a/22039673
        public static int ToArabicNumeral(this string romanNumeral)
        {
            var romanNumberDictionary = new Dictionary<char, int>
            {
                {'I', 1},
                {'V', 5},
                {'X', 10},
                {'L', 50},
                {'C', 100},
                {'D', 500},
                {'M', 1000},
            };

            int total = 0;
            char previousRoman = '\0';

            for (int i = 0; i < romanNumeral.Length; i++)
            {
                var currentRoman = romanNumeral[i];

                var previous = previousRoman != '\0' ? romanNumberDictionary[previousRoman] : '\0';
                var current = romanNumberDictionary[currentRoman];

                if (previous != 0 && current > previous)
                {
                    total = total - (2 * previous) + current;
                }
                else
                {
                    total += current;
                }

                previousRoman = currentRoman;
            }

            return total;
        }

        public static bool IsNumeric(this object expression)
        {
            if (expression == null)
                return false;
            double number;
            return double.TryParse(Convert.ToString(expression, CultureInfo.InvariantCulture), System.Globalization.NumberStyles.Any, NumberFormatInfo.InvariantInfo, out number);
        }
    }
}