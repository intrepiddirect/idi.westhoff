﻿namespace Idi.Core
{
    /// <summary>
    /// Utility to get the base path of the executing application.
    /// </summary>
    public interface IPathProvider
    {
        /// <summary>
        /// Gets the base path of the appolication.  For web apps, this should be the root phyisical directory that the web app is installed into eg: c:\inetpub\www\myapp
        /// </summary>
        /// <returns></returns>
        string GetBasePath();

        /// <summary>
        /// Gets the path to the binaries.  For a console app, this would generally be the same path as <see cref="GetBasePath"/> For a web app it would be the bin folder.
        /// </summary>
        /// <returns></returns>
        string GetExecutablePath();
    }
}