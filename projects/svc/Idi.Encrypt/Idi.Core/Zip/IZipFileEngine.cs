﻿using System.Collections.Generic;
using System.IO;

namespace Idi.Core.Zip
{
    public interface IZipFileEngine
    {
        MemoryStream ZipMemoryStreams(Dictionary<string, MemoryStream> memoryStreams);
    }
}