﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace Idi.Core.Zip
{
    public class ZipFileEngine : IZipFileEngine
    {
        public MemoryStream ZipMemoryStreams(Dictionary<string, MemoryStream> memoryStreams)
        {
            var outputMemStream = new MemoryStream();
            using (var zipStream = new ZipOutputStream(outputMemStream))
            {
                // 0-9, 9 being the highest level of compression
                zipStream.SetLevel(3);

                // Stop ZipStream.Dispose() from also Closing the underlying stream.
                zipStream.IsStreamOwner = false;

                foreach (var memoryStream in memoryStreams)
                {
                    var newEntry = new ZipEntry(memoryStream.Key) { DateTime = DateTime.Now };

                    zipStream.PutNextEntry(newEntry);
                    memoryStream.Value.Seek(0, SeekOrigin.Begin);
                    StreamUtils.Copy(memoryStream.Value, zipStream, new byte[4096]);
                    zipStream.CloseEntry();
                }
            }

            outputMemStream.Position = 0;
            return outputMemStream;
        }
    }
}
