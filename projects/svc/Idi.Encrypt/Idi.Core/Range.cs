﻿namespace Idi.Core
{
    public class Range
    {
        public decimal? Min { get; set; }

        public decimal? Max { get; set; }
    }
}
