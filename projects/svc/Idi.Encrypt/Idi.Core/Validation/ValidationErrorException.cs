﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Idi.Core.Validation
{
    /// <summary>
    /// Conveys validation messages that are acceptable for transmission outside of the current domain (for example, from client to server)
    /// </summary>
    public class ValidationErrorException: Exception
    {
        public IEnumerable<ValidationMessage> ValidationMessages { get; }

        public ValidationErrorException(IEnumerable<ValidationMessage> validationMessages): base(BuildMessage(null, validationMessages))
        {
            ValidationMessages = validationMessages ?? Enumerable.Empty<ValidationMessage>();
        }

        public ValidationErrorException(string message, IEnumerable<ValidationMessage> validationMessages): base(BuildMessage(message, validationMessages))
        {
            ValidationMessages = validationMessages ?? Enumerable.Empty<ValidationMessage>();
        }

        public ValidationErrorException(string message, Exception innerException, IEnumerable<ValidationMessage> validationMessages) : base(BuildMessage(message, validationMessages), innerException)
        {
            ValidationMessages = validationMessages ?? Enumerable.Empty<ValidationMessage>();
        }

        protected ValidationErrorException(SerializationInfo info, StreamingContext context) : base(info, context){ }

        private static string BuildMessage(string message, IEnumerable<ValidationMessage> validationMessages)
        {
            var builder = new StringBuilder();
            if (!string.IsNullOrEmpty(message))
                builder.AppendLine(message);

            if (validationMessages != null && validationMessages.Any())
            {
                builder.AppendLine("Validation Messages");
                foreach (var validationMessage in validationMessages)
                    builder.AppendLine(validationMessage.ToString());
            }

            return builder.ToString();
        }
    }
}
