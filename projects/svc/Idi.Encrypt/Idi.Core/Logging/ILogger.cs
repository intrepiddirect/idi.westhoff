﻿using System;
using System.Linq;

namespace Idi.Core.Logging
{
    // note:  this should in a common, easily referenced assembly -- no ties to a specific logging implementation

    /// <summary>
    /// Logs messages to a customizable sink.
    /// </summary>
    /// <remarks>
    /// Taken from the Ninject.Extensions.Logging project.  I didn't want to reference the project for logging becuase I did not want for 
    /// our base logging contract to  require a reference to Ninject.
    /// </remarks>
    public interface ILogger
    {
        /// <summary>
        /// Gets the name of the logger.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets a value indicating whether messages with Debug severity should be logged.
        /// </summary>
        bool IsDebugEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether messages with Info severity should be logged.
        /// </summary>
        bool IsInfoEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether messages with Warn severity should be logged.
        /// </summary>
        bool IsWarnEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether messages with Error severity should be logged.
        /// </summary>
        bool IsErrorEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether messages with Fatal severity should be logged.
        /// </summary>
        bool IsFatalEnabled { get; }

        /// <summary>
        /// Logs the specified exception with Debug severity.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception">Optional The exception to log.</param>
        /// <param name="elapsedTime">Optional.  If you have timing statistics, put them in this parameter instead of embedding them into <paramref name="message"/></param>
        void Debug(string message, Exception exception = null, TimeSpan? elapsedTime = null);

        /// <summary>
        /// Logs the specified exception with Info severity.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception">Optional The exception to log.</param>
        /// <param name="elapsedTime">Optional.  If you have timing statistics, put them in this parameter instead of embedding them into <paramref name="message"/></param>
        void Info(string message, Exception exception = null, TimeSpan? elapsedTime = null);

        /// <summary>
        /// Logs the specified exception with Warn severity.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception">Optional The exception to log.</param>
        /// <param name="elapsedTime">Optional.  If you have timing statistics, put them in this parameter instead of embedding them into <paramref name="message"/></param>
        void Warn(string message, Exception exception = null, TimeSpan? elapsedTime = null);

        /// <summary>
        /// Logs the specified exception with Error severity.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception">Optional The exception to log.</param>
        /// <param name="elapsedTime">Optional.  If you have timing statistics, put them in this parameter instead of embedding them into <paramref name="message"/></param>
        void Error(string message, Exception exception = null, TimeSpan? elapsedTime = null);

        /// <summary>
        /// Logs the specified exception with Fatal severity.
        /// </summary>
        /// <param name="message">The message or format template.</param>
        /// <param name="exception">Optional The exception to log.</param>
        /// <param name="elapsedTime">Optional.  If you have timing statistics, put them in this parameter instead of embedding them into <paramref name="message"/></param>
        void Fatal(string message, Exception exception = null, TimeSpan? elapsedTime = null);
    }
}