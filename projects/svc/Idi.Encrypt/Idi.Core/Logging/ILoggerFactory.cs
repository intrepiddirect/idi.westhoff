﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idi.Core.Logging
{
    /// <summary>
    /// Factory for Loggers
    /// </summary>
    /// <remarks>Taken from Ninject.Extensions.Logging.  I didn't want to reference the project for logging becuase I did not want for 
    /// our base logging contract to  require a reference to Ninject.
    /// </remarks>
    public interface ILoggerFactory
    {
        /// <summary>
        /// Gets the logger for the specified type, creating it if necessary.
        /// </summary>
        /// <param name="type">The type to create the logger for.</param>
        /// <returns>The newly-created logger.</returns>
        ILogger GetLogger(Type type);

        /// <summary>
        /// Gets a custom-named logger for the specified type, creating it if necessary.
        /// </summary>
        /// <param name="name">The explicit name to create the logger for.  If null, the type's FullName will be used.</param>
        /// <returns>The newly-created logger.</returns>
        ILogger GetLogger(string name);
        
        /// <summary>
        /// Gets a custom-named logger for the current type, creating it if necessary.
        /// </summary>
        /// <returns>The newly-created logger.</returns>
        ILogger GetCurrentClassLogger();
    }
}
