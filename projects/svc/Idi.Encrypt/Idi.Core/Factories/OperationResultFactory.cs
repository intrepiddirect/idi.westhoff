﻿using System;

namespace Idi.Core.Factories
{
    public static class OperationResultFactory
    {
        public static OperationResult OK()
        {
            return new OperationResult
            {
                Succeeded = true
            };
        }

        public static OperationResult<T> OK<T>(T result) where T : class
        {
            return new OperationResult<T>
            {
                Succeeded = true,
                Result = result
            };
        }

        public static OperationResult Fail(string message)
        {
            return new OperationResult
            {
                Succeeded = false,
                Message = message
            };
        }

        public static OperationResult Fail(string message, Exception exception)
        {
            return new OperationResult
            {
                Succeeded = false,
                Message = message,
                Error = exception
            };
        }

        public static OperationResult<T> Fail<T>(string message) where T : class
        {
            return new OperationResult<T>
            {
                Succeeded = false,
                Message = message
            };
        }

        public static OperationResult<T> Fail<T>(string message, Exception exception)
        {
            return new OperationResult<T>
            {
                Succeeded = false,
                Message = message,
                Error = exception
            };
        }

        public static OperationResult<T> Fail<T>(T result, string message)
        {
            return new OperationResult<T>
            {
                Result = result,
                Succeeded = false,
                Message = message
            };
        }

        public static OperationResult<T> Fail<T>(T result, string message, Exception exception)
        {
            return new OperationResult<T>
            {
                Result = result,
                Succeeded = false,
                Message = message,
                Error = exception
            };
        }

        public static OperationResult<T> Fail<T>(OperationResult operationResult)
        {
            return new OperationResult<T>
            {
                Succeeded = false,
                Message = operationResult.Message,
                Error = operationResult.Error
            };
        }
    }
}