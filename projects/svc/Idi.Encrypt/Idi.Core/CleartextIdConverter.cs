﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Idi.Core
{
    /// <summary>
    /// This implementation of IIdConverter simply converts an int id to a clear text string and back again
    /// </summary>
    /// <seealso cref="Idi.Core.IIdConverter" />
    public class CleartextIdConverter: IIdConverter
    {
        public string Encrypt(int id)
        {
            return id.ToString();
        }

        public string Encrypt(int? id)
        {
            return id.HasValue ? Encrypt(id.Value) : null;
        }

        /// <summary>
        /// Returns zero for Whitespace or empty strings
        /// </summary>
        public int Decrypt(string id)
        {
            return string.IsNullOrWhiteSpace(id) ? 0 : int.Parse(id);
        }

        public int? DecryptNullable(string id)
        {
            return string.IsNullOrWhiteSpace(id) ? default(int?) : int.Parse(id);
        }

        public long Decryptlong(string id)
        {
            throw new NotImplementedException();
        }
    }
}
