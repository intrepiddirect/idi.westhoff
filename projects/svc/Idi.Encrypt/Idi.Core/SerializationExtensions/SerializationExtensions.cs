﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Idi.Core.SerializationExtensions
{
    public static class SerializationExtensions
    {
        /// <summary>
        /// returns a datetime that that reflects the date, at 12:00am in the UTC timezone -- so that it will serailize as "TheDateT00:00:00Z"
        /// The difference between using this method and using DateTime.UtcNow is that UtcNow will adjust the date based on the timezone, so with UtcNow, at 11:00pm, the date could be incremented by one day 
        /// With this method, the value of the date will not be changed, the timezone information will be changed.
        /// </summary>
        /// <param name="DateTime">The date time.</param>
        /// <returns></returns>
        public static DateTime AsUtcDate(this DateTime dateTime)
        {
            return DateTime.SpecifyKind(dateTime.Date, DateTimeKind.Utc);
        }

        /// <summary>
        /// Removes unsafe filesystem characters and replaces them with underscores. 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string AsSafeFilename(this string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
                return filename;

            var invalidChars = Path.GetInvalidFileNameChars();
            foreach (var c in invalidChars)
            {
                filename = filename.Replace(c, '_');
            }
            
            return Regex.Replace(filename, @"_{2,}", "_");
        }
    }
}
