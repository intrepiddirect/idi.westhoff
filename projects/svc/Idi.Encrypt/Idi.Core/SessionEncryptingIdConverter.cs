﻿using System.Security.Principal;
using Idi.Core.Encryption;

namespace Idi.Core
{
    public class SessionEncryptingIdConverter: IIdConverter
    {
        private readonly IEncryptionUtility _encryptionUtility;
        private readonly string _sessionToken;
        

        public SessionEncryptingIdConverter(IEncryptionUtility encryptionUtility)
        {
            _encryptionUtility = encryptionUtility;
            _sessionToken = "IDIMKT";
        }

        public string Encrypt(int id)
        {
            return _encryptionUtility.Encrypt(id.ToString(),  _sessionToken).SafeEncodeBase64();
        }

        public string Encrypt(int? id)
        {
            return id.HasValue ? Encrypt(id.Value) : null;
        }

        public int Decrypt(string id)
        {
            return string.IsNullOrWhiteSpace(id) ? 0 : int.Parse(_encryptionUtility.Decrypt(id.SafeDecodeBase64(), _sessionToken));
        }

        public long Decryptlong(string id)
        {
            return string.IsNullOrWhiteSpace(id) ? 0 : long.Parse(_encryptionUtility.Decrypt(id.SafeDecodeBase64(), _sessionToken));
        }

        public int? DecryptNullable(string id)
        {
            string decrypted = _encryptionUtility.Decrypt(id.SafeDecodeBase64(),  _sessionToken);

            return string.IsNullOrEmpty(decrypted) ? (int?) null : int.Parse(decrypted);
        }
    }
}
