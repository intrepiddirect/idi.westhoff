﻿using Idi.Address.Data;
using Idi.Address.Models.Dtos;
using Idi.AddressApi.Client.Handlers;
using Idi.AddressApi.Client.Models;
using Idi.Mcdonalds.Data;
using McDonalds.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace McDonalds.Addess.Preloader.Console
{
    public class ProcessManager
    {
        private readonly McDonaldsRepository _mcdRepo;
        private readonly AddressRepository _addressRepo;
        private readonly IAddressApiClient _addressApiClient;
        private readonly GoogleClient _googleClient;
        private readonly AddressToRepository _addressToRepository;

        public ProcessManager(McDonaldsRepository mcdRepo, AddressRepository addressRepo, IAddressApiClient addressApiClient, GoogleClient googleClient, AddressToRepository addressToRepository)
        {
            _mcdRepo = mcdRepo;
            _addressRepo = addressRepo;
            _addressApiClient = addressApiClient;
            _googleClient = googleClient;
            _addressToRepository = addressToRepository;
        }

        public async Task TestAsync()
        {
            try
            {
                //var addressnew = await _addressApiClient.AddNewAddressAsync(new Idi.AddressApi.Client.Models.AddressRequest
                //{
                //    AddressLine1 = "214 Glenview",
                //    City = "Lawrence",
                //    State = "KS",
                //    ZipCode = "66049",
                //    EffectiveDate = DateTime.Now
                //});
                //Log.Information("New {@a}", addressnew);

                //var address1 = await _addressApiClient.GetByAddressAsync(new Idi.AddressApi.Client.Models.AddressRequest
                //{
                //    AddressLine1 = "1424 Freedom Ln",
                //    City = "Ft Collins",
                //    State = "CO",
                //    ZipCode = "80526",
                //    EffectiveDate = DateTime.Now
                //});
                //Log.Information("By address {@a}", address1);

                var addressList = await _addressApiClient.GetAddressesByIdsAsync(new List<string>
                {
                    "6845836731036618752",
                    "6848323477062176768",
                    "6848325755227754496",
                    "6848326062003343360"
                });
                Log.Information("Many {@a}", addressList);

                var byId = await _addressApiClient.GetAddressByIdAsync("6845836731036618752");
                Log.Information("By Id {@a}", byId);

                //var refreshProp = await TryCallAsync(_addressApiClient.RefreshPropertyDataAsync(6848326062003343360));
                //var refreshRisk = await TryCallAsync(_addressApiClient.RefreshRiskDataAsync(6848326062003343360));
                //var refreshFlood = await TryCallAsync(_addressApiClient.RefreshFloodDataAsync(6848326062003343360));

            }
            catch (Exception ex)
            {
                Log.Error(ex, "You failed");
            }
        }


        public async Task<T> TryCallAsync<T>(Task<T> task) where T : new()
        {
            var returnValue = default(T);
            try
            {
                returnValue = await task;
            }
            catch (IdiClientException ex)
            {
                Log.Error(ex, "You got a client exception, handle it");
            }
            Log.Information("Refresh {name} {@v}", typeof(T).Name, returnValue);
            return returnValue;
        }

        //public async Task RunOfficesAsync()
        //{
        //    Log.Information("Pulling Offices...");
        //    var offices = (await _mcdRepo.GetUnmappedOfficesAsync());
        //    Log.Information($" Ofices count = {offices.Count}"); 

        //    var batchSize = 3;
        //    var numberOfBatches = offices.Count % batchSize == 0 ? offices.Count / batchSize : (offices.Count / batchSize) + 1;
        //    Log.Information($"Starting API calls...");
        //    var completedCount = 0;
        //    var watch = new System.Diagnostics.Stopwatch();
        //    watch.Start();

        //    for (int i = 0; i < numberOfBatches; i++)
        //    {
        //        Log.Information($"Processing batch {i + 1} of {numberOfBatches}...");
        //        var batchOffices = offices.Skip(i * batchSize).Take(batchSize);

        //        var officeAddress = new Dictionary<int, Task<AddressModel>>();

        //        foreach (var office in batchOffices)
        //        {
        //            var addressRequest = new AddressRequest
        //            {
        //                AddressLine1 = office.Officeaddress1,
        //                City = office.Officecity,
        //                State = office.Officestcd,
        //                ZipCode = office.Officezip,
        //                EffectiveDate = new DateTime(2022, 3, 1)
        //            };

        //            officeAddress.Add(office.Id, _addressApiClient.AddNewAddressAsync(addressRequest));
        //        }

        //        var taskList = officeAddress.Values.ToList();
        //        var batchCompletedCount = 0;
        //        while (batchCompletedCount < officeAddress.Count)
        //        {
        //            Task<AddressModel> completedTask = null;
        //            int officeId = -1;
        //            try
        //            {
        //                completedTask = await Task.WhenAny(taskList);
        //                taskList.Remove(completedTask);
        //                officeId = officeAddress.Where(kvp => kvp.Value.Equals(completedTask)).Select(kvp => kvp.Key).FirstOrDefault();
        //            }
        //            catch (Exception ex)
        //            {
        //                Log.Error(ex, "An exception occured trying to await the api call.");
        //            }

        //            try
        //            {
        //                var addressModel = completedTask?.Result;

        //                if (addressModel != null)
        //                {
        //                    var addressId = addressModel.Id;

        //                    Log.Information("*********** \tOfficeId:{tOfficeId}\tAddressId: {addressId}", officeId, addressId);

        //                    await _mcdRepo.AddOfficeAddressMappingAsync(officeId, addressId);

        //                    // add manual BUR record
        //                    await _addressRepo.AddBurManualRecordAsync(addressId);
        //                }
        //                else
        //                {
        //                    Log.Warning("Address Api returned a null address model?? for officeId {officeId}", officeId);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Log.Error(ex, "Failure after Address Api was called for Store Number {storeNumber}", officeId);
        //            }

        //            batchCompletedCount++;
        //            completedCount++;
        //            var elapsed = watch.ElapsedMilliseconds;
        //            decimal average = ((decimal)completedCount / ((decimal)(elapsed))) * 60000;
        //            var minutes = (offices.Count - completedCount) / average;
        //            var now = DateTime.Now.AddMinutes((int)(Math.Round(minutes)));
        //            Log.Information($"Completed {completedCount} of {offices.Count}. Averaging {average} per minute. Estimated completion {now:F}");
        //        }
        //    }
        //}

        //public async Task RunWarehousesAsync()
        //{
        //    Log.Information("Pulling Warehouses...");
        //    var warehouses = (await _mcdRepo.GetUnmappedWarehousesAsync());
        //    Log.Information($"Warehouse count = {warehouses.Count}");

        //    var batchSize = 3;
        //    var numberOfBatches = warehouses.Count % batchSize == 0 ? warehouses.Count / batchSize : (warehouses.Count / batchSize) + 1;
        //    Log.Information($"Starting API calls...");
        //    var completedCount = 0;
        //    var watch = new System.Diagnostics.Stopwatch();
        //    watch.Start();

        //    for (int i = 0; i < numberOfBatches; i++)
        //    {
        //        Log.Information($"Processing batch {i + 1} of {numberOfBatches}...");
        //        var batchWarehouses = warehouses.Skip(i * batchSize).Take(batchSize);

        //        var warehouseAddress = new Dictionary<int, Task<AddressModel>>();

        //        foreach (var warehouse in batchWarehouses)
        //        {
        //            var addressRequest = new AddressRequest
        //            {
        //                AddressLine1 = warehouse.Warehouseaddress1,
        //                City = warehouse.Warehousecity,
        //                State = warehouse.Warehousestcd,
        //                ZipCode = warehouse.Warehousezip,
        //                EffectiveDate = new DateTime(2022, 3, 1)
        //            };

        //            warehouseAddress.Add(warehouse.Id, _addressApiClient.AddNewAddressAsync(addressRequest));
        //        }

        //        var taskList = warehouseAddress.Values.ToList();
        //        var batchCompletedCount = 0;
        //        while (batchCompletedCount < warehouseAddress.Count)
        //        {
        //            Task<AddressModel> completedTask = null;
        //            int warehouseId = -1;
        //            try
        //            {
        //                completedTask = await Task.WhenAny(taskList);
        //                taskList.Remove(completedTask);
        //                warehouseId = warehouseAddress.Where(kvp => kvp.Value.Equals(completedTask)).Select(kvp => kvp.Key).FirstOrDefault();
        //            }
        //            catch (Exception ex)
        //            {
        //                Log.Error(ex, "An exception occured trying to await the api call.");
        //            }

        //            try
        //            {
        //                var addressModel = completedTask?.Result;

        //                if (addressModel != null)
        //                {
        //                    var addressId = addressModel.Id;

        //                    Log.Information("*********** \tWarehouseId:{WarehouseId}\tAddressId: {addressId}", warehouseId, addressId);

        //                    await _mcdRepo.AddWarehouseAddressMappingAsync(warehouseId, addressId);

        //                    // add manual BUR record
        //                    await _addressRepo.AddBurManualRecordAsync(addressId);
        //                }
        //                else
        //                {
        //                    Log.Warning("Address Api returned a null address model?? for warehouseId {warehouseId}", warehouseId);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Log.Error(ex, "Failure after Address Api was called for warehouseId {storeNumber}", warehouseId);
        //            }

        //            batchCompletedCount++;
        //            completedCount++;
        //            var elapsed = watch.ElapsedMilliseconds;
        //            decimal average = ((decimal)completedCount / ((decimal)(elapsed))) * 60000;
        //            var minutes = (warehouses.Count - completedCount) / average;
        //            var now = DateTime.Now.AddMinutes((int)(Math.Round(minutes)));
        //            Log.Information($"Completed {completedCount} of {warehouses.Count}. Averaging {average} per minute. Estimated completion {now:F}");
        //        }
        //    }
        //}

        public async Task RunStoresAsync()
        {
            Log.Information("Pulling Stores...");
            var stores = await _mcdRepo.GetUnmappedStoresAsync();
            Log.Information($"Stores count = {stores.Count}");
            var addresses = new Dictionary<string, AddressRequest>();
            foreach (var store in stores)
            {
                var addressRequest = new AddressRequest
                {
                    AddressLine1 = store.Storeaddress1,
                    City = store.Storecity,
                    State = store.Storestcd,
                    ZipCode = store.Storezip,
                    EffectiveDate = new DateTime(2022, 3, 1)
                };
                addresses.Add(store.Natlstorenbr, addressRequest);
            }

            await ProcessAddressesAsync(addresses);
        }


        public async Task<Dictionary<string, long>> ProcessAddressesAsync(
            Dictionary<string, AddressRequest> addressRequests,
            bool runAllEnrichments = false,
            Action<string, long> UpdateAddressMapping = default,
            Action SaveAction = default)
        {
            var returnValue = new Dictionary<string, long>();
            Log.Information("Pulling Flood Mapping...");
            var floodMappings = await _mcdRepo.GetAllFloodMappingsAsync(addressRequests.Keys);

            var batchSize = 5;
            var numberOfBatches = addressRequests.Count % batchSize == 0 ? addressRequests.Count / batchSize : (addressRequests.Count / batchSize) + 1;
            Log.Information($"Starting API calls...");
            var completedCount = 0;
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            for (int i = 0; i < numberOfBatches; i++)
            {
                Log.Information($"Processing batch {i + 1} of {numberOfBatches}...");
                var batchRequests = addressRequests.Skip(i * batchSize).Take(batchSize);

                var AddressResponseTasks = new Dictionary<string, Task<AddressResponse>>();
                var options = new AddressRequestOptions
                {
                    RunFlood = runAllEnrichments,
                    RunPeril = runAllEnrichments,
                    RunProperty = runAllEnrichments
                };

                foreach (var request in batchRequests)
                {
                    AddressResponseTasks.Add(request.Key, _addressApiClient.AddAddressAsync(request.Value, options));
                }

                var taskList = AddressResponseTasks.Values.ToList();
                var batchCompletedCount = 0;
                while (batchCompletedCount < AddressResponseTasks.Count)
                {
                    var completedTask = await Task.WhenAny(taskList);
                    taskList.Remove(completedTask);
                    var locationId = AddressResponseTasks.Where(kvp => kvp.Value.Equals(completedTask)).Select(kvp => kvp.Key).FirstOrDefault();
                    try
                    {
                        var addressResponse = completedTask.Result;

                        if (addressResponse.Address != null)
                        {
                            var addressModel = addressResponse.Address;
                            var addressId = addressModel.Id;

                            Log.Information("*********** \tLocationId:{locationId}\tAddressId: {addressId}\tIssues: {issues}", locationId, addressId, string.Join(" | ", addressResponse.Errors));

                            returnValue.Add(locationId, addressId);
                            UpdateAddressMapping?.Invoke(locationId, addressId);
                            //await _mcdRepo.AddStoreAddressMappingAsync(locationId, addressId);

                            var floodMapping = floodMappings.FirstOrDefault(f => f.StoreNum.PadLeft(5, '0').Equals(locationId.PadLeft(5, '0'), StringComparison.OrdinalIgnoreCase));
                            if (floodMapping != null)
                            {
                                await _addressRepo.AddFloodDataAsync(addressId, floodMapping);
                            }
                            // add manual BUR record
                            await _addressRepo.AddBurManualRecordAsync(addressId);
                        }
                        else
                        {
                            Log.Warning("Address Api call failed for LocationId Id {storeNumber}. Errors: {errors}", locationId, string.Join(" | ", addressResponse.Errors));
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Failure after Address Api was called for Location Id {storeNumber}", locationId);
                    }
                    batchCompletedCount++;
                    completedCount++;
                    var elapsed = watch.ElapsedMilliseconds;
                    decimal average = ((decimal)completedCount / ((decimal)(elapsed))) * 60000;
                    var minutes = (addressRequests.Count - completedCount) / average;
                    var now = DateTime.Now.AddMinutes((int)(Math.Round(minutes)));
                    Log.Information($"Completed {completedCount} of {addressRequests.Count}. Averaging {average} per minute. Estimated completion {now:U}");
                }
                SaveAction?.Invoke();
            }
            return returnValue;
        }

        public async Task CreateOfficeWarehouseBrandLocationInsertFile()
        {
            var warehouseHashes = await _mcdRepo.GetWarehouseMappings();
            var officeHashes = await _mcdRepo.GetOfficeMappings();

            using var file = File.Open("c:\\Temp\\BrandLocationInsert.sql", FileMode.Create);
            using var writer = new StreamWriter(file);
            writer.WriteLine("DECLARE @user int\r\nEXECUTE @user = [migration].[spGetAccountIdByEmail] 'rwesthoff@intrepiddirect.com'");
            writer.WriteLine("INSERT INTO [dbo].[BrandLocation] ([BrandId], [AddressId], [ExternalLocationId], [LocationAlias], [CreatedDate], [CreatedBy])");
            writer.WriteLine("VALUES");
            foreach (var hash in warehouseHashes.Concat(officeHashes))
            {
                var sql = $"(415, {hash.AddressId}, '{hash.Hash}', '{hash.Alias}', SYSUTCDATETIME(), @user),";
                writer.WriteLine(sql);
            }

        }

        public async Task<bool> RehashAddresses()
        {
            var addresses = await _addressRepo.GetAddressesAsync();
            Parallel.ForEach(addresses, address =>
            {
                address.Hash = GetAddressHash(address);
            });
            return await _addressRepo.SaveAsync();
        }

        public async Task ReprocessBadAddressWithGoogle()
        {
            var stores = await _mcdRepo.GetStoreLatLongByStoreNumber2020(BadStoreAddresses.BadStores);
            var missing = BadStoreAddresses.BadStores.Except(stores.Keys);
            var stores2019 = await _mcdRepo.GetStoreLatLongByStoreNumber2019(missing);
            foreach (var store in stores2019)
            {
                var googleJson = await _googleClient.GetAsync(store.Value.lat, store.Value.lng);
                if (!string.IsNullOrWhiteSpace(googleJson))
                {
                    Log.Information("Found Google Address for Store {id}", store.Key);
                    await _mcdRepo.AddGoogleData(new GoogleAddressDatum
                    {
                        StoreNumber = store.Key.Value,
                        Lat = store.Value.lat,
                        Long = store.Value.lng,
                        GoogleResponseJson = googleJson
                    });
                }
                else
                {
                    Log.Warning("DID NOT find Google Address for Store {id}", store.Key);
                }
            }
        }

        public async Task ProcessGoogleAddresses()
        {
            var googleAddresses = await _mcdRepo.GetAllGoogleAddressesAsync();
            var requests = new Dictionary<string, AddressRequest>();
            foreach (var googleAddress in googleAddresses)
            {
                var json = JObject.Parse(googleAddress.GoogleResponseJson);

                var addressRequest = GetAddressRequestFromGoogleJson(json, 0, googleAddress.StoreNumber);
                if (addressRequest != null)
                {
                    var addressString = $"{addressRequest.AddressLine1}, {addressRequest.City}, {addressRequest.State}, {addressRequest.ZipCode}";
                    Log.Information("Found all address components: {id}, {request}", googleAddress.StoreNumber, addressString);
                    requests.Add(googleAddress.StoreNumber.ToString(), addressRequest);
                }
                else
                {
                    Log.Error("Could not create AddressRequest for google address id: {id}", googleAddress.StoreNumber);
                }
            }

            //var dictionary = await ProcessAddressesAsync(requests, true, (googleId, addressId) =>
            //{
            //    var a = googleAddresses.FirstOrDefault(a => a.StoreNumber.ToString() == googleId);
            //    a.AddressId = addressId;
            //    Log.Information("Updating google address: id = {id}, addressId = {id2}", a.Id, a.AddressId);

            //}, async () => await _mcdRepo.SaveChangesAsync());
        }

        public async Task TransferAddresses()
        {
            var addressToTransfer = await _addressRepo.GetAddressesAsync();
            foreach (var address in addressToTransfer)
            {
                var existing = await _addressToRepository.FindByHash(address.Hash);
                if (existing != null)
                {
                    Log.Warning("Address hash already exists. hash {hash}, fromId {id1}, toId {id2}", existing.Hash, address.Id, existing.Id);
                    continue;
                }

                existing = await _addressToRepository.GetById(address.Id);

                if (existing != null)
                {
                    Log.Warning("Address Id already exists. fromId {id1}, toId {id2}", address.Id, existing.Id);
                    continue;
                }
                Log.Information("Adding {id} to Bridge", address.Id);
                foreach (var entity in address.FloodZones)
                {
                    entity.Id = 0;
                    if (entity.FloodZoneJson != null)
                    {
                        entity.FloodZoneJson.FloodZoneId = 0;
                    }
                }
                address.FloodZones = address.FloodZones.OrderByDescending(f => f.CreatedDate).Take(1).ToList();
                foreach (var entity in address.PropertyEnrichments)
                {
                    entity.Id = 0;
                }
                foreach (var entity in address.PropertyRisks)
                {
                    entity.Id = 0;
                    foreach (var peril in entity.PropertyRiskPerils)
                    {
                        peril.Id = 0;
                    }
                }
                foreach (var entity in address.PropertySearchResults)
                {
                    entity.Id = 0;
                    foreach (var bur in entity.BurEnrichments)
                    {
                        bur.Id = 0;
                    }
                    foreach (var lcr in entity.LcrEnrichments)
                    {
                        lcr.Id = 0;
                    }
                    foreach (var occ in entity.PropertyOccupants)
                    {
                        occ.Id = 0;
                    }
                }
                await _addressToRepository.AddAddress(address);
                Log.Information("Finished adding {id} to Bridge", address.Id);
            }

        }

        public async Task ReprocessFloodZones()
        {
            Log.Information("Loading flood zones and mappings");
            var floodZones = await _addressRepo.GetFloodZones();
            var mappings = await _addressRepo.GetFloodZoneMappings();
            Log.Information("Load done");

            var updates = new List<FloodZone>();

            foreach (var floodZone in floodZones)
            {
                var updated = false;
                if (floodZone.Response != null)
                {
                    floodZone.FloodZoneJson = new FloodZoneJson { Json = floodZone.Response };
                    floodZone.Response = null;
                    updated = true;
                }
                try
                {
                    var floodResponse = JsonSerializer.Deserialize<NationFloodDataResponse>(floodZone.FloodZoneJson.Json);

                    if (floodResponse.Result == null)
                    {
                        Log.Error("Failed to deserialize json. FloodZone Id: {id}", floodZone.Id);
                        continue;
                    }

                    var maxFloodZone = (from hazard in floodResponse.Result.FloodZoneHazards
                                        join fm in mappings on hazard.FloodZone?.Trim()?.ToUpper() equals fm.FloodZone.ToUpper() into fmJoin
                                        from fm2 in fmJoin.DefaultIfEmpty(new Idi.Address.Data.FloodZoneMapping { Rank = 10, FloodZoneShort = "Z" })
                                        orderby fm2.Rank
                                        select fm2.FloodZoneShort).FirstOrDefault();
                    if (maxFloodZone == null)
                    {
                        Log.Error("Did not find a max flood zone. FloodZone Id: {id}", floodZone.Id);
                        continue;
                    }

                    if (maxFloodZone != floodZone.MaxFloodZone)
                    {
                        Log.Warning("FloodZone: Id {id}; Old Max {old}; New Max {new}", floodZone.Id, floodZone.MaxFloodZone, maxFloodZone);
                        floodZone.MaxFloodZone = maxFloodZone;
                        updated = true;
                    }

                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error on FloodZone: {id}", floodZone.Id);
                }
                finally
                {
                    if (updated)
                    {
                        updates.Add(floodZone);
                    }
                }
            }

            try
            {
                var changes = await _addressRepo.UpdateFloodZones(updates);
                Log.Information("Records in update list {updates}. Changes saved {changes}", updates.Count, changes);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to save.");
            }

        }

        private AddressRequest GetAddressRequestFromGoogleJson(JObject json, int resultIndex, int storeNumber)
        {
            AddressRequest addressRequest = null;
            if (json["results"].Count() > resultIndex)
            {
                var addressComponents = json["results"][resultIndex]["address_components"];
                var streetnumber = GetAddressComponentByType(addressComponents, "street_number");
                var street = GetAddressComponentByType(addressComponents, "route");
                var city = GetAddressComponentByType(addressComponents, "locality", "administrative_area_level_3", "sublocality_level_1");
                var state = GetAddressComponentByType(addressComponents, "administrative_area_level_1");
                var zipCode = GetAddressComponentByType(addressComponents, "postal_code");

                if (string.IsNullOrWhiteSpace(streetnumber))
                {
                    //Log.Error("No streetnumber: {id}", storeNumber);
                    addressRequest = GetAddressRequestFromGoogleJson(json, resultIndex + 1, storeNumber);
                }
                else if (string.IsNullOrWhiteSpace(street))
                {
                    //Log.Error("No street: {id}", storeNumber);
                    addressRequest = GetAddressRequestFromGoogleJson(json, resultIndex + 1, storeNumber);
                }
                else if (string.IsNullOrWhiteSpace(city))
                {
                    //Log.Error("No city: {id}", storeNumber);
                    addressRequest = GetAddressRequestFromGoogleJson(json, resultIndex + 1, storeNumber);
                }
                else if (string.IsNullOrWhiteSpace(state))
                {
                    //Log.Error("No state: {id}", storeNumber);
                    addressRequest = GetAddressRequestFromGoogleJson(json, resultIndex + 1, storeNumber);
                }
                else if (string.IsNullOrWhiteSpace(zipCode))
                {
                    //Log.Error("No zipCode: {id}", storeNumber);
                    addressRequest = GetAddressRequestFromGoogleJson(json, resultIndex + 1, storeNumber);
                }
                else
                {
                    addressRequest = new AddressRequest
                    {
                        AddressLine1 = streetnumber + " " + street,
                        City = city,
                        State = state,
                        ZipCode = zipCode,
                        EffectiveDate = new DateTime(2022, 3, 1)
                    };
                }
            }
            return addressRequest;
        }

        private string GetAddressComponentByType(JToken token, params string[] types)
        {
            string value = null;
            foreach (var type in types)
            {
                value = token.Where(a => a["types"].Values<string>().Contains(type)).Select(o => o["long_name"].Value<string>()).FirstOrDefault();
                if (!string.IsNullOrEmpty(value)) break;
            }
            return value;

        }

        private string GetAddressHash(Address address)
        {
            var lat = address.Latitude.HasValue ? address.Latitude.Value : 0;
            var lon = address.Longitude.HasValue ? address.Longitude.Value : 0;
            var hashData = Regex.Replace($"{address.AddressLine1}{address.City}{address.County}{address.StateCode}{address.ZipCode}{Math.Round(lat, 4)}{Math.Round(lon, 4)}", @"\s", "");
            var bytes = Encoding.UTF8.GetBytes(hashData.ToUpper());
            using (var sha256 = SHA256.Create())
            {
                var hashedBytes = sha256.ComputeHash(bytes);
                StringBuilder hash = new StringBuilder();
                for (int i = 0; i < hashedBytes.Length; i++)
                {
                    hash.Append(hashedBytes[i].ToString("X2"));
                }
                return hash.ToString();
            }
        }
    }
}

