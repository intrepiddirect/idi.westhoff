﻿using Serilog;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace McDonalds.Addess.Preloader.Console
{
    public class GoogleClient
    {
        private readonly HttpClient _client;

        public GoogleClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<string> GetAsync(decimal? lat, decimal? lng)
        {
            try
            {
                var response = await _client.GetAsync($"?latlng={lat},{lng}");
                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Google Api exception for lat: {lat}, long {long}", lat, lng);
                return null;
            }
        }

    }

    public class GoogleApiKeyDelegate : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var builder = new UriBuilder(request.RequestUri);
            builder.Query += $"&key=AIzaSyBKyPAYd_OGzEA-zq35Byp9ifSzyyDppBc";

            request.RequestUri = builder.Uri;
            return base.SendAsync(request, cancellationToken);
        }
    }
}
