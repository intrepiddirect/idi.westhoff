﻿using Idi.Address.Data;
using Idi.AddressApi.Client;
using Idi.Http.Authentication;
using Idi.Http.Authentication.ClientCredentials;
using Idi.Mcdonalds.Data;
using McDonalds.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading.Tasks;

namespace McDonalds.Addess.Preloader.Console
{
    class Program
    {
        public async static Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().CreateLogger();
            var host = CreateHostBuilder(args).Build();
            var app = host.Services.GetRequiredService<ProcessManager>();

            //await app.RunAsync();
            //await app.TestAsync();
            //await app.RunOfficesAsync();
            //await app.RunWarehousesAsync();
            //await app.CreateOfficeWarehouseBrandLocationInsertFile();
            //var success = await app.RehashAddresses();
            //Log.Information(success ? "Success" : "Failure");

            //await app.ReprocessBadAddressWithGoogle();
            //await app.ProcessGoogleAddresses();           
            //await app.TransferAddresses();
            await app.ReprocessFloodZones();
            Log.Information("************ DONE!!!! ****************");
            await host.RunAsync();
        }

        public static void ConfigureServices(IServiceCollection serviceCollection)
        {
            IConfiguration Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
            serviceCollection.AddScoped<ProcessManager>();
            serviceCollection.AddDbContext<McdonaldsAccuarialContext>(builder =>
            {
                builder.UseSqlServer(Configuration.GetConnectionString("McDonalds"));
            });
            serviceCollection.AddScoped<McDonaldsRepository>();
            serviceCollection.AddOptions<PreloadOptions>("Options");
            serviceCollection.AddDbContext<AddressContext>(builder =>
            {
                builder.UseSqlServer(Configuration.GetConnectionString("Address"));
            });
            serviceCollection.AddDbContext<AddressToContext>(builder =>
            {
                builder.UseSqlServer(Configuration.GetConnectionString("AddressTo"));
            });

            serviceCollection.AddScoped<AddressRepository>();
            serviceCollection.AddScoped<AddressToRepository>();

            serviceCollection.AddAddressApiClient(config => Configuration.Bind("AddressApi", config));

            serviceCollection.AddHttpClient<GoogleClient>(options =>
            {
                options.BaseAddress = new Uri("https://maps.googleapis.com/maps/api/geocode/json");
            }).AddHttpMessageHandler<GoogleApiKeyDelegate>();

            //serviceCollection.AddTransient<GoogleClient>();
            serviceCollection.AddTransient<GoogleApiKeyDelegate>();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args).UseSerilog((context, configureLogger) =>
               configureLogger.ReadFrom.Configuration(context.Configuration))
                .ConfigureServices(ConfigureServices);

    }
}
