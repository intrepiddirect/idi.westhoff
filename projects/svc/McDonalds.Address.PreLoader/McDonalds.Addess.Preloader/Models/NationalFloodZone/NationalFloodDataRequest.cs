﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class NationalFloodDataRequest
    {
        /// <summary>
        /// This is set during the client call
        /// </summary>
        public string SearchType { get; set; }
        /// <summary>
        /// String version of the address with One comma between <c>City</c> and <c>State</c>. No other punctuation.
        /// <c>Zipcode</c> should be 5 digits only.
        /// </summary>
        public string Address { get; set; }
        public decimal? Latitutde { get; set; }
        public decimal? Longitude { get; set; }
        public bool IncludeLoma { get; set; }
        public bool IncludeElevation { get; set; }
        public bool IncludeProperty { get; set; }
        public bool IncludeParcel { get; set; }

        public Dictionary<string, string> ToQueryParamsDictionary()
        {
            return new Dictionary<string, string>
            {
                { "searchtype", SearchType },
                { "address", Address },
                { "lat", Latitutde?.ToString() },
                { "lng", Longitude?.ToString() },
                { "loma", IncludeLoma.ToString().ToLower() },
                { "elevation", IncludeElevation.ToString().ToLower() },
                { "property", IncludeProperty.ToString().ToLower() },
                { "parcel", IncludeParcel.ToString().ToLower() }
            };
        }
    }
}
