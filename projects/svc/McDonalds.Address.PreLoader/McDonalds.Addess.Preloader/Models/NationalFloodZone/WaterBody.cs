﻿using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class WaterBody
    {
        [JsonPropertyName("areaskm")]
        public string AreaInSqKm { get; set; }

        [JsonPropertyName("distkm")]
        public decimal? DistanceInKm { get; set; }

        [JsonPropertyName("gnis_id")]
        public string GnisId { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("objectid")]
        public string ObjectId { get; set; }

        [JsonPropertyName("ogc_fid")]
        public int OgcFid { get; set; }

        [JsonPropertyName("state")]
        public string State { get; set; }
    }
}
