﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class NationFloodDataResponse
    {
        [JsonPropertyName("geocode")]
        public GeoCode GeoCode { get; set; }

        [JsonPropertyName("coords")]
        public Coordinates Coordinates { get; set; }

        [JsonPropertyName("result")]
        public FloodDataResult Result { get; set; }    
        
        [JsonPropertyName("request")]
        public NationalFloodDataRequest Request { get; set; }
        [JsonIgnore]
        public string JsonResponse { get; set; }
    }
}
