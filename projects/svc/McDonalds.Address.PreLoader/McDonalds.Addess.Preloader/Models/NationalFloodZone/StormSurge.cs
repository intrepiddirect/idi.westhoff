﻿using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class StormSurge
    {
        [JsonPropertyName("1")]
        public decimal? Category1Estimate { get; set; }

        [JsonPropertyName("2")]
        public decimal? Category2Estimate { get; set; }

        [JsonPropertyName("3")]
        public decimal? Category3Estimate { get; set; }

        [JsonPropertyName("4")]
        public decimal? Category4Estimate { get; set; }

        [JsonPropertyName("5")]
        public decimal? Category5Estimate { get; set; }
    }
}
