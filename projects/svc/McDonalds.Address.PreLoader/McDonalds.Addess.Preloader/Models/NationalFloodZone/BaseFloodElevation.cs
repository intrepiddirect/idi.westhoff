﻿using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class BaseFloodElevation
    {
        [JsonPropertyName("bfe_ln_id")]
        public string BfeLnId { get; set; }
        
        [JsonPropertyName("bfe_type")]
        public string BfeType { get; set; }
        
        [JsonPropertyName("dfirm_id")]
        public string FloodInsuranceRateMapId { get; set; }
        
        [JsonPropertyName("distkm")]
        public decimal? DistanceInKm { get; set; }
                
        [JsonPropertyName("elevation")]
        public string Elevation { get; set; }
        
        [JsonPropertyName("fld_ar_id")]
        public string FloodArId { get; set; }
        
        [JsonPropertyName("fld_zone")]
        public string FloodZone { get; set; }
        
        [JsonPropertyName("len_unit")]
        public string LengthUnit { get; set; }
        
        [JsonPropertyName("v_datum")]
        public string VDatum { get; set; }
        
        [JsonPropertyName("zone_subty")]
        public string Description { get; set; }
    }
}
