﻿using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class FloodZoneHazard
    {
        [JsonPropertyName("fld_ar_id")]
        public string PolygonId { get; set; }

        [JsonPropertyName("version_id")]
        public string VerisonId { get; set; }

        [JsonPropertyName("sfha_tf")]
        public string SpecialFloodArea { get; set; }

        [JsonPropertyName("zone_subty")]
        public string Description { get; set; }

        [JsonPropertyName("source_cit")]
        public string Source { get; set; }

        [JsonPropertyName("fld_zone")]
        public string FloodZone { get; set; }

        [JsonPropertyName("dfirm_id")]
        public string FloodInsuranceRateMapId { get; set; }
    }
}
