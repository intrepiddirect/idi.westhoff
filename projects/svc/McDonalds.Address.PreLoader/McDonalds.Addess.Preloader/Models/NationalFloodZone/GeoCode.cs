﻿using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class GeoCode
    {
        [JsonPropertyName("Relevance")]
        public decimal? Revelance { get; set; }

        [JsonPropertyName("Latitude")]
        public decimal? Latitude { get; set; }

        [JsonPropertyName("Longitude")]
        public decimal? Longitude { get; set; }

        [JsonPropertyName("MatchLevel")]
        public string MatchLevel { get; set; }

        [JsonPropertyName("Label")]
        public string GeoAddress { get; set; }  
    }
}
