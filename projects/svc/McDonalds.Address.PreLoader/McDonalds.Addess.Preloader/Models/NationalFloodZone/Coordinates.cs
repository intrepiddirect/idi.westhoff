﻿using System.Text.Json.Serialization;

namespace Idi.Address.Models.Dtos
{
    public class Coordinates
    {   
        /// <summary>
        /// If the Request.SearchType == "addresscoord", this is a decimal
        /// If the Request.SearchType == "addressparcel", this is a string
        /// </summary>
        [JsonPropertyName("lat")]
        public object Latitude { get; set; }
        /// <summary>
        /// If the Request.SearchType == "addresscoord", this is a decimal
        /// If the Request.SearchType == "addressparcel", this is a string
        /// </summary>
        [JsonPropertyName("lng")]
        public object Longitude { get; set; }
    }
}
