﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class _2021Store
    {
        public string OperatorEid { get; set; }
        public string Natlstorenbr { get; set; }
        public string Ownershipstart { get; set; }
        public string Operatorid { get; set; }
        public string Operatornamefirst { get; set; }
        public string Operatornamelast { get; set; }
        public string Verfopername { get; set; }
        public string Newoperatornamefirst { get; set; }
        public string Newoperatornamelast { get; set; }
        public string Storeaddress1 { get; set; }
        public string Storeaddress2 { get; set; }
        public string Storecity { get; set; }
        public string Storestcd { get; set; }
        public string Storezip { get; set; }
        public string Fein { get; set; }
        public string Storecounty { get; set; }
        public string Companynamestore { get; set; }
        public string Companytype { get; set; }
        public string Storetype { get; set; }
        public string Addstoretype { get; set; }
        public string Seating { get; set; }
        public string Parking { get; set; }
        public string Storebuilt { get; set; }
        public string Lastrebuild { get; set; }
        public string Sqfoot { get; set; }
        public string Basement { get; set; }
        public string Addbuildings { get; set; }
        public string Addbuildingsdesc { get; set; }
        public string Constructiontype { get; set; }
        public string _24hrop { get; set; }
        public string If24op { get; set; }
        public string Autosprinkler { get; set; }
        public string Burgalarmcent { get; set; }
        public string Firealarmcent { get; set; }
        public string Haveplayplace { get; set; }
        public string Playplace { get; set; }
        public string Playland { get; set; }
        public string Secguard { get; set; }
        public string Secguardunarmed { get; set; }
        public string Secguardarmed { get; set; }
        public string Secguardcontracted { get; set; }
        public string Seccamera { get; set; }
        public string Seccameraindoor { get; set; }
        public string Seccameraoutdoor { get; set; }
        public string Annualsales { get; set; }
        public string Estguestcount { get; set; }
        public string Delesgguestcount { get; set; }
        public string Delannualsales { get; set; }
        public string Drivethru { get; set; }
        public string Drivethruguestcount { get; set; }
        public string Cleanhoodduck { get; set; }
        public string Ansulsystem { get; set; }
        public string Fryerreplace { get; set; }
        public string Nonmcdops { get; set; }
        public string Ifnonmcdops { get; set; }
        public string Alcoholuse { get; set; }
        public string Liqtotsale { get; set; }
        public string Liqdescsold { get; set; }
        public string Liqhoursold { get; set; }
        public string Liqdrivethru { get; set; }
        public string Liqempdrink { get; set; }
        public string Liqempexp { get; set; }
        public string Liqhostevt { get; set; }
        public string Liqhostexp { get; set; }
        public string Liqlicsup { get; set; }
        public string Liqlicexp { get; set; }
        public string Liqemptrain { get; set; }
        public string Liqtrainexp { get; set; }
        public string Liqintcus { get; set; }
        public string Liqintexp { get; set; }
        public string Liqintsign { get; set; }
        public string Liqsignexp { get; set; }
        public string Liqphtid { get; set; }
        public string Liqphtexp { get; set; }
        public string Liqctl { get; set; }
        public string Liqctlexp { get; set; }
        public string Deliver { get; set; }
        public string Delmore5 { get; set; }
        public string Delavgrad { get; set; }
        public string Delemp { get; set; }
        public string Delwho { get; set; }
        public string Delinscar { get; set; }
        public string Delseccar { get; set; }
        public string Deldrilic { get; set; }
        public string Deldrirec { get; set; }
        public string Deldrivehrec { get; set; }
        public string Delwriproc { get; set; }
        public string Armoredcare { get; set; }
        public string Depositstobank { get; set; }
        public string Insured { get; set; }
        public string Lobbytreatyes { get; set; }
    }
}
