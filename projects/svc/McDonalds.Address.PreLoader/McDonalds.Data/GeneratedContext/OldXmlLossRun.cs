﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class OldXmlLossRun
    {
        public string Fn { get; set; }
        public string CompanyName { get; set; }
        public string Claimnbr { get; set; }
        public string Storenbr { get; set; }
        public string Cvg { get; set; }
        public DateTime? AccdntDate { get; set; }
        public decimal? IncurredTotal { get; set; }
        public decimal? PaidTotal { get; set; }
        public string ClaimDescription { get; set; }
        public string ExperienceRatingInclusion { get; set; }
    }
}
