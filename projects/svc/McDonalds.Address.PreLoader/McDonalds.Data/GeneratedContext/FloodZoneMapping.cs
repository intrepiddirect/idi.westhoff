﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class FloodZoneMapping
    {
        public int Id { get; set; }
        public string FloodZone { get; set; }
        public string FloodZoneShort { get; set; }
        public int? Rank { get; set; }
    }
}
