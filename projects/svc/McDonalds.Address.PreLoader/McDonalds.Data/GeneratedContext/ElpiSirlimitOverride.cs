﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class ElpiSirlimitOverride
    {
        public string KontrolNbr { get; set; }
        public string OperatorNm { get; set; }
        public string CompanyNm { get; set; }
        public string AddressLine1 { get; set; }
        public string CityNm { get; set; }
        public string StateCd { get; set; }
        public string PostalCd { get; set; }
        public decimal? PreviousYearSirlimit { get; set; }
        public decimal? CurrentYearSirlimit { get; set; }
        public string CurrentSiryear { get; set; }
        public string ExceptionReasonTxt { get; set; }
        public string ActionRequiredTxt { get; set; }
    }
}
