﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class LossrunsCurrent
    {
        public string Valuation { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? LossDate { get; set; }
        public string DescriptionClean { get; set; }
        public int? Storenumber { get; set; }
        public string Coverage { get; set; }
        public double? IncurredCase { get; set; }
        public double? IncurredTotal { get; set; }
        public double? PaidRecovery { get; set; }
        public double? PaidTotal { get; set; }
        public int? PolicyYear { get; set; }
        public int? AccY { get; set; }
        public decimal? CpTrendCumulative { get; set; }
        public decimal? GlTrendCumulative { get; set; }
        public double? _100kCappedIncLoss { get; set; }
        public double? _250kCappedIncLoss { get; set; }
        public string CatFlag { get; set; }
    }
}
