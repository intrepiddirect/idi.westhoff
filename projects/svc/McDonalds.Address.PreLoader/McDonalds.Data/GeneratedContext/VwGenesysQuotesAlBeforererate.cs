﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VwGenesysQuotesAlBeforererate
    {
        public int BatchNum { get; set; }
        public string PolicyNo { get; set; }
        public string UnitType { get; set; }
        public string UnitDesc { get; set; }
        public string UnitAddress { get; set; }
        public string UnitCity { get; set; }
        public string UnitState { get; set; }
        public string UnitZipcode { get; set; }
        public decimal? PropertyPremium { get; set; }
        public decimal? PropertyPremiumExclBgii { get; set; }
        public decimal? LiabilityPremium { get; set; }
        public decimal? PropertyBuildingLimit { get; set; }
        public decimal? PropertyBpplimit { get; set; }
        public decimal? PropertyTelimit { get; set; }
        public decimal? Gldisc { get; set; }
        public decimal? Gltier { get; set; }
        public decimal? Cpdisc { get; set; }
        public decimal? Cptier { get; set; }
        public decimal? ManualPropertyPremium { get; set; }
        public decimal? ManualPropertyPremiumExclBgii { get; set; }
        public decimal? ManualLiabilityPremium { get; set; }
    }
}
