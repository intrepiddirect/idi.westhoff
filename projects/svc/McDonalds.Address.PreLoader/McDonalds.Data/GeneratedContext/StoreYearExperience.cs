﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class StoreYearExperience
    {
        public DateTime? Dt { get; set; }
        public int? StoreNumber { get; set; }
        public int? HasLocationType { get; set; }
        public int? HasAggData2020 { get; set; }
        public int? HasAggData2019 { get; set; }
        public int? HasAggData2018 { get; set; }
        public int? Has2020Sales { get; set; }
        public string SiteTypeDesc { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? Zip { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Aug2020Sales { get; set; }
        public decimal? Aug2020Gc { get; set; }
        public decimal? Aug2020DtsalesPct { get; set; }
        public decimal? Aug2020DeliverySalesPct { get; set; }
        public string Aug2020FranchiseeNm { get; set; }
        public long? Aug2020FranchiseeId { get; set; }
        public string StoreHours { get; set; }
        public string DriveThruHours { get; set; }
        public string DriveThrough { get; set; }
        public string Wifi { get; set; }
        public string MobileDeals { get; set; }
        public string MobileOrders { get; set; }
        public string IndoorPlayplace { get; set; }
        public string IndoorPlayground { get; set; }
        public string OutdoorPlayPlaceAvailable { get; set; }
        public string IndoorDiningAvailable { get; set; }
        public string GiftCards { get; set; }
        public string WalmartLocation { get; set; }
        public string CreateYourTasteAvailable { get; set; }
        public string Mcdelivery { get; set; }
        public string Storebeginningdate { get; set; }
        public string Storeendingdate { get; set; }
        public int? StoreNumberDt { get; set; }
        public decimal? PropIncurredTotal { get; set; }
        public decimal? PropIncurredCappedTotal { get; set; }
        public int? PropClaimCount { get; set; }
        public decimal? GlIncurredTotal { get; set; }
        public decimal? GlIncurredCappedTotal { get; set; }
        public int? GlClaimCount { get; set; }
        public decimal? MondayStoreHours { get; set; }
        public decimal? TuesdayStoreHours { get; set; }
        public decimal? WednesdayStoreHours { get; set; }
        public decimal? ThursdayStoreHours { get; set; }
        public decimal? FridayStoreHours { get; set; }
        public decimal? SaturdayStoreHours { get; set; }
        public decimal? SundayStoreHours { get; set; }
        public decimal? MondayDthours { get; set; }
        public decimal? TuesdayDthours { get; set; }
        public decimal? WednesdayDthours { get; set; }
        public decimal? ThursdayDthours { get; set; }
        public decimal? FridayDthours { get; set; }
        public decimal? SaturdayDthours { get; set; }
        public decimal? SundayDthours { get; set; }
    }
}
