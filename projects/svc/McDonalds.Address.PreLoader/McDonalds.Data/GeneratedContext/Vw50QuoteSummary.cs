﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Vw50QuoteSummary
    {
        public int BatchNum { get; set; }
        public string Ver { get; set; }
        public decimal? ClientId { get; set; }
        public string InsuredName { get; set; }
        public string PolicySymbolC { get; set; }
        public string PolicyNo { get; set; }
        public decimal? PolicyNbrSeq { get; set; }
        public int? Locations { get; set; }
        public decimal? BasicGlpremium { get; set; }
        public decimal? BasicPremPremium { get; set; }
        public decimal? BasicProdPremium { get; set; }
        public string GlexperienceRatingEligible { get; set; }
        public decimal? TotalGlpremium { get; set; }
        public decimal? TotalCppremium { get; set; }
        public decimal? TotalPkgPremium { get; set; }
        public decimal? GlincurredLossRuns { get; set; }
        public decimal? CpincurredLossRuns { get; set; }
        public double? ModelPackageFactor { get; set; }
        public decimal? BasicPkgPremium { get; set; }
        public double? ModelPremium { get; set; }
    }
}
