﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class IsoGlStategroup
    {
        public int Id { get; set; }
        public string State { get; set; }
        public string Abbreviation { get; set; }
        public int? Code { get; set; }
        public string StateGroup { get; set; }
    }
}
