﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VotingDatum
    {
        public string Year { get; set; }
        public string State { get; set; }
        public string StatePo { get; set; }
        public string CountyName { get; set; }
        public string CountyFips { get; set; }
        public string Office { get; set; }
        public string Candidate { get; set; }
        public string Party { get; set; }
        public int? Candidatevotes { get; set; }
        public int? Totalvotes { get; set; }
        public string Version { get; set; }
        public string Mode { get; set; }
    }
}
