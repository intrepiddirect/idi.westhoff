﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class CoverageMap
    {
        public int? CoverageCode { get; set; }
        public string Coverage { get; set; }
        public string CoverageDescription { get; set; }
    }
}
