﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class GoogleAddressDatum
    {
        public int Id { get; set; }
        public int StoreNumber { get; set; }
        public decimal? Lat { get; set; }
        public decimal? Long { get; set; }
        public string GoogleResponseJson { get; set; }
        public long? AddressId { get; set; }
    }
}
