﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class StoreCrimeScore
    {
        public int? StoreNumber { get; set; }
        public string StateCrimeCategory { get; set; }
    }
}
