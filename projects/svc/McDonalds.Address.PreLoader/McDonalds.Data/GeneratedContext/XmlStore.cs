﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class XmlStore
    {
        public long? Id { get; set; }
        public string StoreNumber { get; set; }
        public string Zip { get; set; }
        public string Ownershipstart { get; set; }
        public string Statename { get; set; }
        public string SiteTypeDesc { get; set; }
        public string Sales { get; set; }
        public string DelPct { get; set; }
        public string Dtpct { get; set; }
        public string CompanyName { get; set; }
    }
}
