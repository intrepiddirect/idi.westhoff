﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class WarehouseAddressMapping
    {
        public int Id { get; set; }
        public int WarehouseId { get; set; }
        public long AddressId { get; set; }
    }
}
