﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class SalesBucketingByRegion
    {
        public string Region { get; set; }
        public int? Low { get; set; }
        public int? Med { get; set; }
    }
}
