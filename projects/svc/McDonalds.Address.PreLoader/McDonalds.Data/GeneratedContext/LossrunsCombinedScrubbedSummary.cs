﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class LossrunsCombinedScrubbedSummary
    {
        public int? Maturity { get; set; }
        public string Valuation { get; set; }
        public string PolicyYear { get; set; }
        public int? StoreNumber { get; set; }
        public string OwnerId { get; set; }
        public string KontrolNumber { get; set; }
        public string Coverage { get; set; }
        public int? OcurrenceCount { get; set; }
        public decimal? IncurredTotal { get; set; }
    }
}
