﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class McDonaldsLocationType
    {
        public string QuoteNbr { get; set; }
        public int UnitCommonId { get; set; }
        public string LocationTypeNm { get; set; }
        public int? BatchNbr { get; set; }
        public string LocationStateCd { get; set; }
    }
}
