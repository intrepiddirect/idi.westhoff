﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Lossruns20142018
    {
        public DateTime? LossDate { get; set; }
        public string ClaimStatus { get; set; }
        public string AccidentDescription { get; set; }
        public decimal? IncurredIndemnity { get; set; }
        public decimal? IncurredTotal { get; set; }
        public decimal? PaidRecovery { get; set; }
        public decimal? PaidTotal { get; set; }
        public string OriginalLineOfBusinsess { get; set; }
        public string PolicyYear { get; set; }
        public string LossState { get; set; }
        public DateTime? ClaimOpenDate { get; set; }
        public DateTime? ClaimClosedDate { get; set; }
        public int Id { get; set; }
        public DateTime? ValuationDate { get; set; }
    }
}
