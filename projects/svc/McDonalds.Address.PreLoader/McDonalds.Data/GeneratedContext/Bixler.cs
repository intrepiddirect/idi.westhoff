﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Bixler
    {
        public string PolicyNbr { get; set; }
        public int UnitcommonId { get; set; }
        public string LocationType { get; set; }
    }
}
