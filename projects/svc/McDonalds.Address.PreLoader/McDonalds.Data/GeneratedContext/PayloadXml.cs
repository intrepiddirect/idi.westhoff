﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class PayloadXml
    {
        public int Id { get; set; }
        public string Fn { get; set; }
        public string Payload { get; set; }
    }
}
