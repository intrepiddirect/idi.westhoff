﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class ModelDataset
    {
        public int? StoreNumber { get; set; }
        public string StoreSource { get; set; }
        public int? Storebuilt { get; set; }
        public int? Lastrebuild { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? Zip { get; set; }
        public string OperatorEid { get; set; }
        public string OperatorName { get; set; }
        public DateTime? Ownershipstart { get; set; }
        public string Storetype { get; set; }
        public string SiteTypeDesc { get; set; }
        public decimal? Seating { get; set; }
        public decimal? Parking { get; set; }
        public string Constructiontype { get; set; }
        public decimal? Sqfoot { get; set; }
        public string Basement { get; set; }
        public string Addbuildings { get; set; }
        public string Companytype { get; set; }
        public string Companynamestore { get; set; }
        public string _24hrop { get; set; }
        public string If24op { get; set; }
        public string Autosprinkler { get; set; }
        public string Burgalarmcent { get; set; }
        public string Firealarmcent { get; set; }
        public string Haveplayplace { get; set; }
        public string Playplace { get; set; }
        public string Playland { get; set; }
        public string Secguard { get; set; }
        public string Seccamera { get; set; }
        public decimal? Annualsales { get; set; }
        public decimal? Estguestcount { get; set; }
        public decimal? Delesgguestcount { get; set; }
        public decimal? Drivethruguestcount { get; set; }
        public string Drivethru { get; set; }
        public decimal? Delannualsales { get; set; }
        public decimal? Dtannualsales { get; set; }
        public string Cleanhoodduck { get; set; }
        public string Ansulsystem { get; set; }
        public string Fryerreplace { get; set; }
        public string Alcoholuse { get; set; }
        public double? Dtsales19 { get; set; }
        public int? OperatorStoreCount { get; set; }
        public string Companyname { get; set; }
        public string LocationType { get; set; }
        public int? StoreBeginningDate { get; set; }
        public int? StoreEndingDate { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Region { get; set; }
        public string IsostateGroup { get; set; }
        public decimal? Ilf250kTo1m { get; set; }
        public string RucaPrimary { get; set; }
        public string RucaSecondary { get; set; }
        public string JudicialProfile { get; set; }
        public decimal? CommuteTime { get; set; }
        public decimal? AtheniumHail { get; set; }
        public decimal? AtheniumSlWind { get; set; }
        public decimal? AtheniumTornado { get; set; }
        public decimal? AtheniumWs { get; set; }
        public decimal? AtheniumIce { get; set; }
        public decimal? AtheniumRain { get; set; }
        public decimal? AtheniumHurricane { get; set; }
        public string FloodZone { get; set; }
        public decimal? Visualscore { get; set; }
        public decimal? AalHurricane { get; set; }
        public decimal? AalEq { get; set; }
        public decimal? ManualLiabilityPremium { get; set; }
        public decimal? ManualPropertyPremium { get; set; }
        public decimal? ManualPropertyPremiumExclBgii { get; set; }
        public decimal? ManualPropertyPremiumBgii { get; set; }
        public decimal? ManualPropertyPremiumModel { get; set; }
        public decimal? PropLimit { get; set; }
    }
}
