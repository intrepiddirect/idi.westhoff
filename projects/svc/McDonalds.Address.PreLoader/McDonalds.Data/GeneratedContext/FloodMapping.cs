﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class FloodMapping
    {
        public int Id { get; set; }
        public DateTime? InsertDt { get; set; }
        public string StoreNum { get; set; }
        public string FloodZone { get; set; }
        public decimal? MinDist { get; set; }
        public string JsonTxt { get; set; }
        public string FloodZoneFixed { get; set; }
    }
}
