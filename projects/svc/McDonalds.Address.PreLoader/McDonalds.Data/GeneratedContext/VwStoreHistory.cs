﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VwStoreHistory
    {
        public int? StoreNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int? ZipCode { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string County { get; set; }
        public string ActiveDt { get; set; }
    }
}
