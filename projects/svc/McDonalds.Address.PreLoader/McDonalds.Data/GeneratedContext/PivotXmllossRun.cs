﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class PivotXmllossRun
    {
        public string Fn { get; set; }
        public string CompanyName { get; set; }
        public string LossRunClaimNumber { get; set; }
        public string LossRunAccdntDate { get; set; }
        public string LossRunAccdntDesc { get; set; }
        public string LossRunClaimNbr { get; set; }
        public string LossRunCovgDesc { get; set; }
        public string LossRunIncurredTotal { get; set; }
        public string LossRunKontrolNbr { get; set; }
        public string LossRunNatlStoreNbr { get; set; }
        public string LossRunPaidTotal { get; set; }
        public string LossRunPolicyEffEndDate { get; set; }
        public string LossRunPolicyEffStartDate { get; set; }
    }
}
