﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class VwVerifyScrub
    {
        public string Valuation { get; set; }
        public decimal? ScrubOffbal { get; set; }
    }
}
