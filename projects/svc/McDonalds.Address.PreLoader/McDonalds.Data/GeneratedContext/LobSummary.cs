﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class LobSummary
    {
        public string PolicyYear { get; set; }
        public string MappedLob { get; set; }
        public decimal? TotalPaid { get; set; }
        public decimal? TotalIncurred { get; set; }
        public int? ClaimCount { get; set; }
        public int? ClaimCountWithIncurred { get; set; }
        public int? ClaimCountWithPayment { get; set; }
    }
}
