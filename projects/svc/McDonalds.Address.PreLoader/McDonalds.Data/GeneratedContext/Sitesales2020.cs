﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class Sitesales2020
    {
        public int? NatlStrNu { get; set; }
        public int? _5DigitNsn { get; set; }
        public int? StSite { get; set; }
        public long? OperId { get; set; }
        public string Operator { get; set; }
        public int? Feid { get; set; }
        public string CompanyName { get; set; }
        public decimal? Aug20TtmSls { get; set; }
        public decimal? Aug20TtmGcs { get; set; }
        public decimal? Aug20TtmDtSls { get; set; }
        public decimal? Aug20TtmDtGcs { get; set; }
        public decimal? DtDeliverySalesAnnualized { get; set; }
        public decimal? DtDeliverySalesAnnualized1 { get; set; }
        public decimal? DtGuestCountsAnnualized { get; set; }
        public decimal? DtGuestCountsAnnualized1 { get; set; }
    }
}
