﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class FloodMappingBkup7232021
    {
        public int Id { get; set; }
        public DateTime? InsertDt { get; set; }
        public string StoreNum { get; set; }
        public string FloodZone { get; set; }
        public decimal? MinDist { get; set; }
        public string JsonTxt { get; set; }
    }
}
