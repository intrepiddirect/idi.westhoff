﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class OldXmlStoreinfo
    {
        public string Fn { get; set; }
        public string CompanyName { get; set; }
        public int? Storenbr { get; set; }
        public DateTime? Ownershipstart { get; set; }
        public int? StorebuiltYear { get; set; }
        public string Statename { get; set; }
    }
}
