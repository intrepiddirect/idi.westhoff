﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Mcdonalds.Data
{
    public partial class XmlLossrunPrepivot
    {
        public string Fn { get; set; }
        public string CompanyName { get; set; }
        public string LossRunClaimNumber { get; set; }
        public string QuestionCode { get; set; }
        public string QuestionAnswer { get; set; }
    }
}
