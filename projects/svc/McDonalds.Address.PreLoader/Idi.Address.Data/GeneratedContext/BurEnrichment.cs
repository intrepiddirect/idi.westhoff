﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class BurEnrichment
    {
        public int Id { get; set; }
        public int PropertySearchResultId { get; set; }
        public string ReportType { get; set; }
        public string ConstructionType { get; set; }
        public int? YearBuilt { get; set; }
        public string Bceg { get; set; }
        public DateTime? SurveyDate { get; set; }
        public int? NumberOfStories { get; set; }
        public bool? Sprinkler { get; set; }
        public decimal? SquareFootage { get; set; }
        public string GroupIiSymbol { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PropertySearchResult PropertySearchResult { get; set; }
    }
}
