﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class PropertyRiskPeril
    {
        public int Id { get; set; }
        public int PropertyRiskId { get; set; }
        public string Peril { get; set; }
        public decimal? Risk { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PropertyRisk PropertyRisk { get; set; }
    }
}
