﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class Address
    {
        public Address()
        {
            FloodZones = new HashSet<FloodZone>();
            PropertyEnrichments = new HashSet<PropertyEnrichment>();
            PropertyRisks = new HashSet<PropertyRisk>();
            PropertySearchResults = new HashSet<PropertySearchResult>();
        }

        public long Id { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string County { get; set; }
        public string FipsCountyCode { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Hash { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StateCode { get; set; }

        public virtual ICollection<FloodZone> FloodZones { get; set; }
        public virtual ICollection<PropertyEnrichment> PropertyEnrichments { get; set; }
        public virtual ICollection<PropertyRisk> PropertyRisks { get; set; }
        public virtual ICollection<PropertySearchResult> PropertySearchResults { get; set; }
    }
}
