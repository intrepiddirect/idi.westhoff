﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class PropertySearchResult
    {
        public PropertySearchResult()
        {
            BurEnrichments = new HashSet<BurEnrichment>();
            LcrEnrichments = new HashSet<LcrEnrichment>();
            PropertyOccupants = new HashSet<PropertyOccupant>();
        }

        public int Id { get; set; }
        public long AddressId { get; set; }
        public string RiskId { get; set; }
        public string MatchTypeCode { get; set; }
        public DateTime? SurveyDate { get; set; }
        public string LocationDescription { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Address Address { get; set; }
        public virtual ICollection<BurEnrichment> BurEnrichments { get; set; }
        public virtual ICollection<LcrEnrichment> LcrEnrichments { get; set; }
        public virtual ICollection<PropertyOccupant> PropertyOccupants { get; set; }
    }
}
