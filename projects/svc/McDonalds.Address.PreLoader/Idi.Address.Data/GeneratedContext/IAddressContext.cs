﻿using Microsoft.EntityFrameworkCore;

namespace Idi.Address.Data
{
    public interface IAddressContext
    {
        DbSet<Address> Addresses { get; set; }
        DbSet<BurEnrichment> BurEnrichments { get; set; }
        DbSet<FloodZoneJson> FloodZoneJsons { get; set; }
        DbSet<FloodZoneMapping> FloodZoneMappings { get; set; }
        DbSet<FloodZone> FloodZones { get; set; }
        DbSet<JudicialProfileMapping> JudicialProfileMappings { get; set; }
        DbSet<LcrEnrichment> LcrEnrichments { get; set; }
        DbSet<PropertyEnrichment> PropertyEnrichments { get; set; }
        DbSet<PropertyOccupant> PropertyOccupants { get; set; }
        DbSet<PropertyRiskPeril> PropertyRiskPerils { get; set; }
        DbSet<PropertyRisk> PropertyRisks { get; set; }
        DbSet<PropertySearchResult> PropertySearchResults { get; set; }
        DbSet<RegionMapping> RegionMappings { get; set; }
    }
}