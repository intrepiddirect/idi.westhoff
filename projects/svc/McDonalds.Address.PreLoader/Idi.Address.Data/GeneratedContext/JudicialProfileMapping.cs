﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class JudicialProfileMapping
    {
        public int Id { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string Profile { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
