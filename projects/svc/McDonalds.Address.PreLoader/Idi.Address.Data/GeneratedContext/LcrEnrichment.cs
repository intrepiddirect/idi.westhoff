﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Idi.Address.Data
{
    public partial class LcrEnrichment
    {
        public int Id { get; set; }
        public int PropertySearchResultId { get; set; }
        public string ReportType { get; set; }
        public string PropertyRatingType { get; set; }
        public decimal? BuildingBg1 { get; set; }
        public decimal? BuildingBg2 { get; set; }
        public decimal? BuildingCauseOfLoss { get; set; }
        public decimal? PersonalPropertyBg1 { get; set; }
        public decimal? PersonalPropertyBg2 { get; set; }
        public decimal? PersonalPropertyCauseOfLoss { get; set; }
        public DateTime? SurveyDate { get; set; }
        public DateTime? VersionEffectiveDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PropertySearchResult PropertySearchResult { get; set; }
    }
}
