﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace TestAppCore
{////asdlfkj
    public class AsyncTiming
    {
        public int? testInt { get; set; }
        public int regInt { get; set; }
        /////some comment
        public AsyncTiming()
        {
            this.testInt = default;
            this.regInt = default;
        }

        public async Task TestExternal(int iterations)
        {
            Console.WriteLine("Starting External...");
            long total = 0;
            for (int r = 0; r < iterations; r++)
            {
                var taskList = new List<Task>();
                var watch = Stopwatch.StartNew();

                for (int i = 0; i < 10; i++)
                {
                    taskList.Add(Run(i));
                }

                await Task.WhenAll(taskList);
                watch.Stop();
                total += watch.ElapsedMilliseconds;
            }
            Console.WriteLine($"\r\nExtrenal loop total = {total}; average = {total / iterations}");

        }
     
        public async Task TestInternal(int loops = 10)
        {
            long totalTimeLoop1 = 0;
            long totalTimeLoop2 = 0;
            var collection1 = new List<string>();
            var collection2 = new List<string>();
            for (int i = 0; i < loops; i++)
            {
                //loop 1
                Console.WriteLine("Starting first...");
                var timer = Stopwatch.StartNew();
                var resposnesAsync = new List<Task<int>>();
                for (int j = 1; j <= 10; j++)
                {
                    resposnesAsync.Add(asyncInt(j));
                }
                await Task.WhenAll(resposnesAsync);

                var responseStringsAsync = new List<Task<string>>();
                foreach (var response in resposnesAsync)
                {
                    responseStringsAsync.Add((await response).GetStringAsync());
                }
                await Task.WhenAll(responseStringsAsync);

                foreach (var responseStringTask in responseStringsAsync)
                {
                    //Console.WriteLine(await responseStringTask);
                    collection1.Add(await responseStringTask);
                }
                timer.Stop();
                totalTimeLoop1 += timer.ElapsedMilliseconds;
                //Console.WriteLine($"First Run time - {timer.ElapsedMilliseconds}");


                // loop 2
                Console.WriteLine("\r\nStarting second...");

                var timer2 = Stopwatch.StartNew();
                for (int j = 1; j <= 10; j++)
                {
                    var num = await asyncInt(j);
                    var response = await num.GetStringAsync();
                    collection2.Add(response);
                    //Console.WriteLine(response);
                }

                timer2.Stop();
                totalTimeLoop2 += timer2.ElapsedMilliseconds;
                //Console.WriteLine($"Second Run time - {timer2.ElapsedMilliseconds}");                 
            }

            Console.WriteLine($"Totals\r\nLoop 1 = {totalTimeLoop1}; Average = {totalTimeLoop1 / loops}\r\nLoop 2 = {totalTimeLoop2}; Average = {totalTimeLoop2 / loops}");
        }

        private async Task Run(int iteration)
        {
            var num = await asyncInt(iteration);
            var response = await num.GetStringAsync();
        }

        private async Task<int> asyncInt(int delay = 1)
        {
            //var time = delay * 500;
            await Task.Delay(Ext.GetDelay());
            return delay;
        }

    }

    public static class Ext
    {
        public static async Task<string> GetStringAsync(this int delay)
        {
            //var time = delay * 500;
            await Task.Delay(GetDelay());
            return "async string - " + delay;
        }

        public static int GetDelay()
        {
            return new Random().Next(100, 300);
        }
    }
}
