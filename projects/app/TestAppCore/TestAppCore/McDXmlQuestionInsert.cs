﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace TestAppCore
{
    public class McDXmlQuestionInsert
    {
        public void CreateInsertStatements()
        {
            var files = Directory.EnumerateFiles(@"C:\Users\rwesthoff\Documents\McDonalds\SampleXML");
            var fs = File.Create(@"C:\Users\rwesthoff\Documents\McDonalds\Insert_Questions.txt");
            fs.Close();
            var questions = new List<Question>();
            foreach (var file in files)
            {
                var xml = File.ReadAllText(file);
                var doc = XDocument.Parse(xml);

                var formNode = doc.Element("Form");

                foreach (var element in formNode.Descendants())
                {

                    var section = element.Name;
                    foreach (var sectionChildren in element.Descendants())
                    {
                        if (sectionChildren.Descendants()
                            .Any(e => e.Name == "QuestionNumber"))
                        {
                            var question = new Question
                            {
                                Name = sectionChildren.Name.ToString(),
                                Section = section.ToString(),
                                QuestionNumber = sectionChildren.Element("QuestionNumber").Value,
                                Prompt = sectionChildren.Element("QuestionText").Value,

                            };

                            questions.Add(question);
                        }
                    }
                }
            }
            questions = questions.Distinct().OrderBy(i => i.Section).ThenBy(i => i.QuestionNumber, new QuestionNumberComparer()).ToList();
            int i = 1;
            foreach (var insert in questions)
            {
                File.AppendAllText(@"C:\Users\rwesthoff\Documents\McDonalds\Insert_Questions.txt", $"({i++}, {insert})," + Environment.NewLine);
            }

            Console.WriteLine("DONE!");
        }
    }

    public class Question
    {
        public string QuestionNumber { get; set; }
        public string Section { get; set; }
        public string Prompt { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (obj as Question == null)
                return false;

            var question = obj as Question;
            return question.Name == Name
                && question.Prompt == Prompt
                && question.Section == Section
                && question.QuestionNumber == QuestionNumber;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Section, QuestionNumber);
        }

        public override string ToString()
        {
            return $@"'{Name}', '{Section}', '{Prompt}', '{QuestionNumber}'";
        }
    }

    public class QuestionNumberComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            var xParts = x.Split('.');
            var yParts = y.Split('.');

            var equal = 0;

            for (int i = 0; i < Math.Max(xParts.Length, yParts.Length); i++)
            {
                int xInt = 0;
                int yInt = 0;
                if (xParts.Length > i)
                {
                    xInt = int.Parse(xParts[i]);
                }

                if (yParts.Length > i)
                {
                    yInt = int.Parse(yParts[i]);
                }

                if (xInt != yInt)
                {
                    equal = xInt > yInt ? 1 : -1;
                    break;
                }
            }

            return equal;
        }
    }
}
