﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace TestAppCore
{
    public class McDonaldsXml
    {
        // just adding a comment
        public void Validate()
        {
            var files = Directory.EnumerateFiles(@"C:\Users\rwesthoff\Documents\McDonalds\SampleXML");
            var fs = File.Create(@"C:\Users\rwesthoff\Documents\McDonalds\XML_Validation_Errors.txt");
            fs.Close();

            foreach (var file in files)
            {
                var xml = File.ReadAllText(file);
                var docx = XDocument.Parse(xml);
                using (var xsdStream = GetXsd())
                {
                    using (var xmlReader = XmlReader.Create(xsdStream))
                    {
                        var schemaSet = new XmlSchemaSet();
                        schemaSet.Add("", xmlReader);

                        docx.Validate(schemaSet, (sender, args) =>
                        {
                            if (args.Exception != null)
                            {
                                var subStrInx = file.LastIndexOf("\\") + 1;
                                var message = $"{file.Substring(subStrInx, file.Length - subStrInx)}: {args.Message}";
                                switch (sender)
                                {
                                    case XElement el:
                                        message += $" Failing XElement: {el}";
                                        break;
                                    default:
                                        message += $"Failing Object: {JsonConvert.SerializeObject(sender)}";
                                        break;
                                }

                                File.AppendAllText(@"C:\Users\rwesthoff\Documents\McDonalds\XML_Validation_Errors.txt", message + Environment.NewLine);
                            }
                        });
                    }
                }
            }

        }


        public void CreatePostmanJsonDataFile()
        {
            var dataFile = @"C:\Users\rwesthoff\Documents\Postman\Data\McD_Xml_Data.postman_data.json";
            var files = Directory.EnumerateFiles(@"C:\Users\rwesthoff\Documents\McDonalds\SampleXML");
            var fs = File.Create(dataFile);
            fs.Close();

            var data = new List<SubmissionData>();
            foreach (var file in files)
            {
                var xmlArray = File.ReadAllLines(file);
                var stringifyXml = string.Join("", xmlArray.Select(x => x.Trim()));
                data.Add(new SubmissionData { Submission = stringifyXml });                
            }

            var json = JsonConvert.SerializeObject(data, Newtonsoft.Json.Formatting.Indented);
            File.AppendAllText(dataFile, json);
        }



        private FileStream GetXsd()
        {
            return File.OpenRead(@"C:\Users\rwesthoff\Documents\McDonalds\McDonaldsXSD_v1.xsd");
        }
    }

    public class SubmissionData
    { 
        public string Submission { get; set; }
    }

}
