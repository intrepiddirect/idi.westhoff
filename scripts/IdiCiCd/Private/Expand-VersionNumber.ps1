<#
    .SYNOPSIS
        Takes a string version number in SemVer format, and returns an object with the parsed verion parts.
#>
function Expand-VersionNumber {
    param (
        [Parameter()] [string] $Version
    )

    $versionMatch = Select-String -InputObject $Version -Pattern "^v?(?<version>(?<major>\d+)(\.(?<minor>\d+|\*))?(\.(?<patch>\d+|\*))?(\-(?<pre>[0-9A-Za-z\-\.]+?))?([\+\.](?<build>[0-9A-Za-z\-\.]+))?)$"

    if ($null -ne $versionMatch) {
        $expanded = $versionMatch | 
            Select-Object -ExpandProperty Matches -First 1 | 
            ForEach-Object {New-Object PSObject -Property @{ 
                Major = Get-CleanVersionSegmentString $_.Groups["major"].Value
                Minor = Get-CleanVersionSegmentString $_.Groups["minor"].Value
                Patch = Get-CleanVersionSegmentString $_.Groups["patch"].Value
                Pre = $_.Groups["pre"].Value
                Build = Get-CleanVersionSegmentString $_.Groups["build"].Value -ZeroValue ""
                Version = $_.Groups["version"].Value
                NugetVersion = ""
                AssemblyVersion = ""
            }}

        $pre = $expanded.Pre
        if (![string]::IsNullOrWhiteSpace($pre)) {
            $pre = "-$pre"
        }

        $expanded.NugetVersion = @($expanded.Major,$expanded.Minor,"$($expanded.Patch)$pre",$expanded.Build).Where({ ![string]::IsNullOrWhiteSpace($_) }) -join "."
        $expanded.AssemblyVersion = $expanded.NugetVersion -replace [regex]::Escape($pre),""

        return $expanded
    }
    return $null
}

function Get-CleanVersionSegmentString {
    param([string]$NumberString, [string]$ZeroValue="0")

    if ([string]::IsNullOrWhiteSpace($NumberString)) {
        return ""
    }
    if ($NumberString -eq "*") {
        return $NumberString
    }
    $num = [int]$NumberString
    if ($num -eq 0) {
        return $ZeroValue
    }
    return $num.ToString()
}
