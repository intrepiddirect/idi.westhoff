function Add-ChangelogEntry {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [psobject] $Commits,
        [string] $Version
    )

    Push-Location (Resolve-Path $Path)
    try {
        $breakingChanges = @()

        $changelogPath = Join-Path "docs" "CHANGELOG.md" | Resolve-VirtualPath
        $changelog = ""
        if (Test-Path $changelogPath) {
            $changelog = Get-Content $changelogPath -Raw
        } else {
            $changelog = "# Changelog`n`nAll notable changes to this project will be documented in this file.`n`nThis project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) and is automated with [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).`n"
        }

        $newLogEntry = ""

        $loggableCommitTypes = @{
            "fix" = "Fixes"
            "feat" = "Features"
            "perf" = "Performance Improvements"
            "style" = "User Experience Improvements"
        }

        $groupedCommits = $Commits | Group-Object -Property Type -AsHashTable

        foreach ($commitType in $groupedCommits.Keys) {
            if ($loggableCommitTypes.ContainsKey($commitType)) {
                $newLogEntry += "`n`n### $($loggableCommitTypes[$commitType])"

                $groupedCommits[$commitType] | ForEach-Object {
                    $newLogEntry += "`n- "
                    if (-not([string]::IsNullOrWhiteSpace($_.Scope))) {
                        $newLogEntry += "**$($_.Scope)**: "
                    }
                    $commitTitle = ($_.Message -split '\n')[0]
                    $newLogEntry += $commitTitle
    
                    $tickets = $_.Message | 
                            Select-String "(?mi)IDI\-\d+" -AllMatches | 
                            Select-Object -ExpandProperty Matches |
                            Select-Object -ExpandProperty Value

                    if ($tickets.Count -gt 0) {
                        $newLogEntry += " ($($tickets -join ", "))"
                    }
                    
                    $_.Message | 
                        Select-String "(?si)BREAKING\sCHANGE:?\s*(?<change>.*)$" -AllMatches | 
                        Select-Object -ExpandProperty Matches |
                        Select-Object -ExpandProperty Groups | 
                        Where-Object { $_.Name -eq "change" } |
                        Select-Object -ExpandProperty Value |
                        ForEach-Object {
                            $breakingChangeMessage = ""
                            if ([string]::IsNullOrWhiteSpace($_)) {
                                $breakingChangeMessage += $commitTitle
                            } else {
                                $breakingChangeMessage += $_
                            }
                            if ($tickets.Count -gt 0) {
                                $breakingChangeMessage += " ($($tickets -join ", "))"
                            }
                            $breakingChanges += $breakingChangeMessage
                        }
                }
            }
        }

        if ($breakingChanges.Count -gt 0) {
            $newLogEntry += "`n`n### **Breaking Changes**"

            $breakingChanges | ForEach-Object {
                $newLogEntry += "`n- $_"
            }
        }

        if ([string]::IsNullOrWhiteSpace($newLogEntry)) {
            Write-Host "No commit messages found to add to changelog"
        } else {
            $newLogEntry = "`n## Release v$Version ($(Get-Date -Format "yyyy-MM-dd"))$newLogEntry"

            if ($changelog -match "\n##\s") {
                [regex]$pattern = "\n##\s"
                $changelog = $pattern.Replace($changelog, "$newLogEntry`n`n## ", 1)
            } else {
                $changelog += $newLogEntry
            }

            if (-not(Test-Path $changelogPath)) {
                New-Item $changelogPath -Force -ItemType File -WhatIf:$WhatIfPreference
            }
            $changelog | Set-Content $changelogPath -WhatIf:$WhatIfPreference
        }
    } finally {
        Pop-Location
    }
}
