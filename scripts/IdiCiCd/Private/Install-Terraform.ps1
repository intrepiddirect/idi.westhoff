function Install-Terraform {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [string]$Version="1.0.1",
        [string]$Architecture="windows_386",
        [switch]$Force=$False
    )

    [Net.ServicePointManager]::SecurityProtocol = "Tls12, Tls11, Tls, Ssl3"

    $repoRoot = Get-RepositoryRoot $Path
    Push-Location $repoRoot
    try {
        if (-not(Test-Path .\tmp\terraform.exe) -or $Force) {
            Invoke-RestMethod -Uri "https://releases.hashicorp.com/terraform/1.0.1/terraform_1.0.1_windows_386.zip" -OutFile "terraform.zip"
            Expand-Archive terraform.zip .\tmp -Force
            Remove-Item "terraform.zip"
        }
        Set-Alias terraform (Resolve-Path .\tmp\terraform.exe).ToString() -Option AllScope -Scope Script

        # install solace terraform provider
        $platform = terraform version -json | ConvertFrom-Json | Select-Object -ExpandProperty platform
        $version = (Invoke-RestMethod -Uri https://api.github.com/repos/PatrickDelancy/terraform-provider-solace/releases/latest | Select-Object -ExpandProperty tag_name).TrimStart("v")        

        $outPath = Join-Path $Env:APPDATA "terraform.d\plugins\intrepiddirect.com\idi\solace\$version\$platform\"

        if (-not(Test-Path $outPath)) {
            Invoke-RestMethod -Uri "https://github.com/PatrickDelancy/terraform-provider-solace/releases/download/v$($version)/terraform-provider-solace_$($version)_$($platform).zip" -OutFile "terraform-provider-solace.zip"

            New-Item $outPath -Type Directory -ErrorAction SilentlyContinue
            
            Expand-Archive terraform-provider-solace.zip $outPath -Force
            Remove-Item "terraform-provider-solace.zip"
        }
    } finally {
        Pop-Location
    }
}
