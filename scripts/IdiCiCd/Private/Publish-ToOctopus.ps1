function Publish-ToOctopus {
    [CmdletBinding()]
    param (
        [string]$OctopusUrl,
        [string]$ApiKey,
        [string]$FilePath
    )

    Add-Type -AssemblyName System.Net.Http
    
    $httpClientHandler = New-Object System.Net.Http.HttpClientHandler
    $httpClient = New-Object System.Net.Http.HttpClient $httpClientHandler
    $httpClient.DefaultRequestHeaders.Add("X-Octopus-ApiKey", $ApiKey)
    
    $fileStream = New-Object System.IO.FileStream($FilePath, [System.IO.FileMode]::Open)
    
    $contentDispositionHeaderValue = New-Object System.Net.Http.Headers.ContentDispositionHeaderValue "form-data"
    $contentDispositionHeaderValue.Name = "fileData"
    $contentDispositionHeaderValue.FileName = [System.IO.Path]::GetFileName($FilePath)
    
    $streamContent = New-Object System.Net.Http.StreamContent $fileStream
    $streamContent.Headers.ContentDisposition = $contentDispositionHeaderValue
    $contentType = "multipart/form-data"
    $streamContent.Headers.ContentType = New-Object System.Net.Http.Headers.MediaTypeHeaderValue $contentType
    
    $content = New-Object System.Net.Http.MultipartFormDataContent
    $content.Add($streamContent)
    
    Write-Host "Publishing file $FilePath to octopus $OctopusUrl"
    $httpClient.PostAsync("$OctopusUrl/api/packages/raw?replace=false", $content).Result
    
    if ($null -ne $fileStream)
    {
        $fileStream.Close()
    }
}
