
function Convert-TfJsonToPmdXml {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True,ValueFromPipeline=$True)] [string]$InputJson
    )

    Write-Verbose "Parsing input json: $InputJson"

    $obj = ConvertFrom-Json $InputJson
    
    $severityMap = @{
        "critical" = 1
        "error" = 2
        "warning" = 3
        "info" = 4
    }

    $errorMap = @{}

    $obj.diagnostics | ForEach-Object {
        $severity = 2
        if ($severityMap.ContainsKey($_.severity)) {
            $severity = $severityMap[$_.severity]
        }

        if (!$errorMap.ContainsKey($_.range.filename)) {
            $errorMap.Add($_.range.filename, @())
        }

        $errorMap[$_.range.filename] += "
        <violation
            beginline=""$($_.range.start.line)"" endline=""$($_.range.end.line)""
            begincolumn=""$($_.range.start.column)"" endcolumn=""$($_.range.end.column)""
            rule=""$($_.summary)"" priority=""$severity"">$($_.detail)</violation>"
    }

    $outputXml = "<?xml version=""1.0"" encoding=""UTF-8""?>
<pmd xmlns=""http://pmd.sourceforge.net/report/2.0.0"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://pmd.sourceforge.net/report/2.0.0 https://pmd.sourceforge.io/report_2_0_0.xsd"">"
    
    $errorMap.Keys | ForEach-Object {
        $outputXml += "
    <file name=""$_"">$($errorMap[$_] -join '')
    </file>"
    }

    $outputXml += "
</pmd>"

    return $outputXml
}
