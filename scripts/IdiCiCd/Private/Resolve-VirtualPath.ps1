<#
.SYNOPSIS
    Calls Resolve-Path but works for files that don't exist.
#>
function Resolve-VirtualPath {
    param (
        [Parameter(Mandatory=$True,ValueFromPipeline=$True)] [string] $Path
    )

    $Path = Resolve-Path $Path -ErrorAction SilentlyContinue -ErrorVariable _resolveError
    if (-not($Path)) {
        $Path = $_resolveError[0].TargetObject
    }

    return $Path
}