<#
    .SYNOPSIS
        Get's the relative folder name for the project root.
#>
function Get-ProjectFolder {
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.'
    )

    return Resolve-Path $Path | 
        Select-String -Pattern "\bprojects[/\\][^/\\]+[/\\][^/\\]+([/\\]|$)" | 
        Select-Object -ExpandProperty Matches -First 1 | 
        Select-Object -ExpandProperty Value
}
