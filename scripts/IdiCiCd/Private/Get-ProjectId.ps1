<#
    .SYNOPSIS
        Get's the unique project id for the project root.
#>
function Get-ProjectId {
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.'
    )

    $fullPath = Resolve-Path $Path

    $match = Select-String -InputObject $fullPath -Pattern "\bprojects[/\\][^/\\]+[/\\](?<dir>[^/\\]+)([/\\]|$)"
    if (!$match) {
        Write-Host "Unable to determine project id from path $fullPath"
        return ""
    }

    $projectPath = $match.Matches[0].Groups["dir"].Value

    $projectId = ($projectPath -replace "(^idi|\W+)","").ToLowerInvariant()

    return $projectId
}
