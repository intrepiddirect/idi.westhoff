<#
    .SYNOPSIS
        Determines what (if any) pre-release flag should be applied based on the given branch name.
#>
function Get-PrereleaseFlag {
    param ([Parameter(Mandatory)]$branch, $prefix="-")

    if ($branch -match "(?:^|/)(master$|release/[^/]+)$") {
        # Master and release branches should not have "pre-release" version
        $preRelease = ""
        #Write-Host "##teamcity[setParameter name='releaseChannel' value='Production']"
    } elseif ($branch -match "(?:^|/)(feature|epic)/.*(IDI-\d+)(?:[^\d]|$)") {
        # Use IDI-<ticket_number> for properly formatted feature/epic branches
        $preRelease = $prefix + $Matches[1] + "-" + $Matches[2]
    } elseif ($branch -match "(?:^|/)(pull-requests)/(\d+)(?:$|/)") {
        # well-known branch types use specific pre-release flags
        $preRelease = $prefix + "pullrequest-" + $Matches[2]
    } elseif ($branch -match "(?:^|/)(develop|feature|epic|bugfix|hotfix)(?:$|/)") {
        # well-known branch types use specific pre-release flags
        $preRelease = $prefix + ($Matches[1] -replace "\W+","")
    } else {
        # builds on unknown branch types are always considered "private" builds
        $preRelease = "$($prefix)private"
    }

    return $preRelease
}
