<#
    .SYNOPSIS
        Installs a local reference for the target project.
#>
function Unregister-LocalRef {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter()] [string] $TargetProject="Idi\.[^""]+",
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.'
    )

    Push-Location (Resolve-Path $Path)
    try {
        $projectFiles = Get-Item "**\*.csproj"
        foreach ($projectFile in $projectFiles) {
            $refBackupFilePath = Join-Path "tmp" "$($projectFile.BaseName).json" | Resolve-VirtualPath
            $refBackup = @{}
            if (Test-Path $refBackupFilePath) {
                (Get-Content -Path $refBackupFilePath -Raw | ConvertFrom-Json).psobject.properties | ForEach-Object { $refBackup[$_.Name] = $_.Value }
            }

            $refKeys = $refBackup.Keys|Where-Object{$_}

            foreach ($idiRefName in $refKeys) {
                if ($idiRefName -match "^$TargetProject$") {
                    if ($PSCmdlet.ShouldProcess("Adding project reference for $($idiRefName) to $projectFile", "TARGET", "OPERATION")) {
                        dotnet add $projectFile package $idiRefName --interactive --no-restore --version $refBackup[$idiRefName]
                    }
                    $refBackup.Remove($idiRefName)
                }
            }

            if ($refBackup.Count -eq 0) {
                if (Test-Path $refBackupFilePath) {
                    Remove-Item $refBackupFilePath -Force -ErrorAction Ignore -WhatIf:$WhatIfPreference
                }
            } else {
                if ($PSCmdlet.ShouldProcess("Updating referenced project backup for $projectFile", "TARGET", "OPERATION")) {
                    $refBackup | ConvertTo-Json | Set-Content -Path $refBackupFilePath -Encoding UTF8 -Force
                }
            }
        }
    } finally {
        Pop-Location
    }
}
