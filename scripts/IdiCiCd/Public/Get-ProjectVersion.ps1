<#
    .SYNOPSIS
        Determines project version by examining project files.

    .DESCRIPTION
        Looks for `package.json`, `AssemblyInfo.cs` or `*.csproj` files and parses the first one found
        in order to determine the project version.
#>
function Get-ProjectVersion {
    param (
        [Parameter(Mandatory=$False)] [string]$Path='.'
    )
    Push-Location $Path

    try {
        if (Test-Path "package.json") {
            $file = Get-Item "package.json"
        } elseif (Test-Path "main.tf") {
            $file = Get-Item "main.tf"
        } else {
            $projectFiles = Get-ChildItem "AssemblyInfo.cs" -Recurse | Where-Object { $_.FullName -notmatch "[\\/](bin|obj)[\\/]" }
            if ($projectFiles.Count -eq 0) {
                $projectFiles = Get-ChildItem "*.csproj" -Recurse | Where-Object { $_.FullName -notmatch "[\\/](bin|obj)[\\/]" }
            }
            if ($projectFiles.Count -gt 0) {
                $file = $projectFiles[0]
            }
        }
        
        if ($null -eq $file) {
            Write-Host "No suitable versioned project file found. Leaving default build number unchanged."
            return $null
        }

        if ($file.Extension -eq ".csproj") {
            $xml = [xml](Get-Content $file.FullName)
            $version = $xml.GetElementsByTagName("Version") | Select-Object -First 1 -ExpandProperty "#text"
        } elseif ($file.Name -eq "AssemblyInfo.cs") {
            $versionMatches = Get-Content $file.FullName -Raw | Select-String "(?m)^\[assembly\:\s*AssemblyVersion\(""(?<version>[^""]*)""\)\]"
            $version = $versionMatches.Matches[0].Groups["version"].Value
        } elseif ($file.Name -eq "package.json") {
            $version = Get-Content $file.FullName -Raw | ConvertFrom-Json | Select-Object -ExpandProperty "version"
        } elseif ($file.Name -eq "main.tf") {
            $versionMatches = Get-Content $file.FullName -Raw | Select-String "(?ms)^(output\s+""version""\s+{.*?value\s*=\s*"")(?<version>[^""]*)("".*?})"
            $version = $versionMatches.Matches[0].Groups["version"].Value
        }

        return Expand-VersionNumber $version
    } finally {
        Pop-Location
    }
}
