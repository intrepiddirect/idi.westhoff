<#
    .SYNOPSIS
        Builds a project
#>
function Invoke-Build {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [Switch]$SkipPackageRestore=$False,
        [string]$BuildConfiguration="Release"
    )

    $config = Get-Configuration $Path -SetEnv

    Push-Location (Resolve-Path $Path)
    try {
        if (Test-Path ".scripts\prebuild.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking pre-build script prebuild.ps1", "TARGET", "OPERATION")) {                
                .".scripts\prebuild.ps1"
            }
        }

        if (Test-Path ".scripts\build.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking build script build.ps1", "TARGET", "OPERATION")) {                
                .".scripts\build.ps1"
            }
        } elseif (Test-Path "package.json" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking NPM build", "TARGET", "OPERATION")) {
                if (!$SkipPackageRestore) {
                    npm ci
                }
                npm run build
            }
        } elseif (Test-Path "*.sln") {
            $projectFiles = Get-Item "**/*.csproj" | Where-Object { $_.BaseName -notmatch "Tests$" }

            foreach ($projectFile in $projectFiles) {
                if ($PSCmdlet.ShouldProcess("Invoking .NET build on $projectFile", "TARGET", "OPERATION")) {
                    if (!$SkipPackageRestore) {
                        dotnet restore $projectFile.FullName --configfile (Resolve-Path (Join-Path (Get-RepositoryRoot) "nuget.config"))
                    }
                    dotnet build $projectFile.FullName --no-restore --configuration $BuildConfiguration --output (Join-Path $config.BuildOutputPath $projectFile.BaseName)
                }
            }
        } elseif (Test-Path "main.tf") {
            $projectName = (Get-Item -Path ".\").BaseName
            $srcPath = (Resolve-Path '.').ToString()
            $outPath = (Join-Path $config.BuildOutputPath $projectName)
            New-Item $outPath -ItemType Directory -ErrorAction Ignore | Out-Null

            Get-ChildItem $srcPath -Recurse | 
                Where-Object { 
                    $_.FullName -notmatch "(\.md|\.terraform|\.tfstate|\.tfstate\.d)(\\|\/|$)"
                } | Copy-Item -Destination {Join-Path $outPath $_.FullName.Substring($srcPath.length)}
        } else {
            Write-Host "Unsupported project type."
        }

        if (Test-Path ".scripts\postbuild.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking post-build script postbuild.ps1", "TARGET", "OPERATION")) {
                .".scripts\postbuild.ps1"
            }
        }
    } finally {
        Pop-Location
    }
}
