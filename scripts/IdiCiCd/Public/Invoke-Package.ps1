<#
    .SYNOPSIS
        Publishes a project
#>
function Invoke-Package {
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$False,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)] [string[]]$Path='.',
        [Switch]$SkipPackageRestore=$False,
        [string]$BuildConfiguration="Release"
    )

    $config = Get-Configuration $Path -SetEnv

    Push-Location (Resolve-Path $Path)
    try {
        if (Test-Path ".scripts\prepackage.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking pre-package script prepackage.ps1 with args $args", "TARGET", "OPERATION")) {
                .".scripts\prepackage.ps1" @args
            }
        }

        if (Test-Path ".scripts\package.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking package script package.ps1 with args $args", "TARGET", "OPERATION")) {
                .".scripts\package.ps1" @args
            }
        } elseif (Test-Path "package.json" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking NPM package", "TARGET", "OPERATION")) {
                if (!$SkipPackageRestore) {
                    npm ci
                }
                npm run package
            }
        } elseif (Test-Path "*.sln") {
            $projectFiles = Get-Item "**/*.csproj" | Where-Object { $_.BaseName -notmatch "Tests$" }
            $version = Get-ProjectVersion
            
            foreach ($projectFile in $projectFiles) {
                if ($PSCmdlet.ShouldProcess("Invoking .NET package or publish on $projectFile", "TARGET", "OPERATION")) {
                    if (!$SkipPackageRestore) {
                        dotnet restore $projectFile --configfile (Resolve-Path (Join-Path (Get-RepositoryRoot) "nuget.config"))
                    }

                    $packable = (Get-Content $projectFile -Raw) -match "<IsPackable>true<\/IsPackable>"
                    if ($packable) {
                        dotnet build $projectFile --no-restore --configuration $BuildConfiguration
                        dotnet pack $projectFile --no-restore --configuration $BuildConfiguration --output $config.PackageOutputPath
                    } else {
                        dotnet publish $projectFile --no-restore --configuration $BuildConfiguration --output (Join-Path $config.PackageOutputPath "$($projectFile.BaseName).$($version.NugetVersion)")
                    }
                }
            }
        } elseif (Test-Path "main.tf") {
            $projectName = (Get-Item -Path ".\").BaseName
            $version = Get-ProjectVersion

            $srcPath = (Resolve-Path '.').ToString()
            $outPath = (Join-Path $config.PackageOutputPath "$projectName.$($version.NugetVersion)")
            New-Item $outPath -ItemType Directory -ErrorAction Ignore | Out-Null

            Get-ChildItem $srcPath -Recurse | 
                Where-Object { 
                    $_.FullName -notmatch "(\.md|\.terraform|\.tfstate(\.d)?|\.tf(vars)?\.[a-zA-Z0-9_\-]+)(\\|\/|$)"
                } | Copy-Item -Destination {Join-Path $outPath $_.FullName.Substring($srcPath.length)}

            Get-Item "*.$BuildConfiguration" |
                ForEach-Object {
                    Copy-Item $_.ToString() -Destination (Join-Path $outPath ($_.Name -replace "\.$BuildConfiguration$","")) -Force
                }
        } else {
            Write-Host "Unsupported project type."
        }

        if (Test-Path ".scripts\postpackage.ps1" -PathType Leaf) {
            if ($PSCmdlet.ShouldProcess("Invoking post-package script postpackage.ps1 with args $args", "TARGET", "OPERATION")) {
                .".scripts\postpackage.ps1" @args
            }
        }
    } finally {
        Pop-Location
    }
}
