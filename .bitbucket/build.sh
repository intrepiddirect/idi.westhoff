#!/bin/bash
# /Users/rwesthoff/AppData/Local/Programs/Git/bin/bash
source .bitbucket/vars/_shared_vars.sh

for i in .bitbucket/scripts/*;
  do source $i
done

echo $BITBUCKET_BUILD_NUMBER
for path in "${PROJECT_PATHS[@]}"
do
  echo "Running build script for $path"
  REPORTS_PATH=./test-reports/build_$BITBUCKET_BUILD_NUMBER              
  dotnet restore ./$path
  # dotnet build ./$path --no-restore --configuration Release
  # dotnet test ./$path --no-build --configuration Release --test-adapter-path:. --logger:"junit;LogFilePath=$REPORTS_PATH/junit.xml"
  projectPath=$path getNextVersion newVersion
  echo "Next version for $path is $newVersion" >> output.txt
done