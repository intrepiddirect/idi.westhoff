export PROJECT_PATHS=()
for i in .bitbucket/vars/*
do
    if [[ $i =~ "_shared_vars.sh" ]]
    then
        continue;
    else
        echo "Adding this file $i"
        source $i
    fi
done